
import os 
import sys
import csv
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import host_subplot
import matplotlib.ticker as ticker
import mpl_toolkits.axisartist as AA
from collections import defaultdict



plt.style.use("seaborn-whitegrid")
#plt.rcParams['font.size'] = 10
#plt.rcParams['font.family'] = "cursive"
plt.rcParams['font.weight']='bold'
plt.rcParams['axes.labelsize'] = 8
plt.rcParams['axes.labelweight'] =  'bold'
plt.rcParams['axes.titlesize'] = 15
plt.rcParams['xtick.labelsize'] = 8
plt.rcParams['ytick.labelsize'] = 7
plt.rcParams['legend.fontsize'] = 8
bar_width=0.1


#command: python plot_cache_results -path -timeseries_file_name -exp_percentage(string 3 digits) -cache_name -size

nuca_map_ptw = {}
nuca_map_utopia = {}

blocks = (2**int(sys.argv[5]))/64
for folder in sorted(os.listdir("results/"+sys.argv[1])):
    if("sls" not in folder):
        if (os.path.isdir("results/"+sys.argv[1]+"/"+folder)):
            if(os.path.exists("results/"+sys.argv[1]+"/"+folder+"/"+sys.argv[2])):
                nuca_map_ptw[folder] = []
                with open("results/"+sys.argv[1]+"/"+folder+"/"+sys.argv[2]) as nuca_file:
                    lines = nuca_file.readlines()
                    for line in lines:
                        nuca_map_ptw[folder].append((((int)(line)*1.0)/(blocks*1.0))*100)

                        

plt.cla()
plt.clf()
    
fig, axs = plt.subplots()
fig.set_size_inches(6,3)

axs.grid(color='black', linestyle='--', linewidth=0.1)

plt.xticks([0,100000,200000,300000,400000,500000,600000,700000,800000], 
            ["0", "100K", "200K", "300K", "400K", "500K","600K","700K","800K"])

plt.xlabel("Epochs (1K instructions interval)", fontsize = 7)
fig.text(0.06, 0.5, "Ratio of " +sys.argv[4]+" cache blocks with \n Page Table Entries (%)", ha='center', va='center', rotation='vertical',fontsize = 8)

apps=1

radix = "Radix"

average_occupancy = []

app_names = []

for folder in sorted(nuca_map_ptw):
    print(folder)
    if(folder.split("_")[-3] == sys.argv[3]):
        app_names.append(folder.split("_")[0])
        average_occupancy.append(np.mean(np.array(nuca_map_ptw[folder])))
        ind  = np.arange(0,len(nuca_map_ptw[folder]),1)
        axs.plot(ind,nuca_map_ptw[folder],linewidth=1,label=folder.split("_")[0])
        apps = apps + 1

print(average_occupancy)
print(app_names)

plt.legend(loc="upper center", ncol =5, fontsize=8,bbox_to_anchor =(0.5, 1.25))

plt.subplots_adjust(hspace=0.5)
plt.savefig("./"+sys.argv[1]+"_"+sys.argv[4]+"_"+sys.argv[3]+"percent.pdf", bbox_inches="tight",dpi=300)
plt.savefig("./"+sys.argv[1]+"_"+sys.argv[4]+"_"+sys.argv[3]+"percent.png", bbox_inches="tight",dpi=300)

# plt.cla()
# plt.clf()

# fig, axs = plt.subplots()
# fig.set_size_inches(6,2)


# axs.bar(np.arange(0,apps-1),average_occupancy,linewidth=1.0,width=0.5,label=app_names)
# plt.xticks(np.arange(0,apps-1),app_names,rotation=40)

# plt.xlabel("Applications", fontsize = 7)
# fig.text(0.06, 0.5, "Ratio of LLC cache blocks with \n Page Table Entries (%)", ha='center', va='center', rotation='vertical',fontsize = 8)

# plt.savefig("./nuca_data_radix_stacked.pdf", bbox_inches="tight",dpi=300)
# plt.savefig("./nuca_data_radix_stacked.png", bbox_inches="tight",dpi=300)

