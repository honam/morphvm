import os 
import sys
import csv
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import host_subplot
import matplotlib.ticker as ticker
import mpl_toolkits.axisartist as AA
from collections import defaultdict

plt.style.use("seaborn-whitegrid")
#plt.rcParams['font.size'] = 10
#plt.rcParams['font.family'] = "cursive"
plt.rcParams['font.weight']='bold'
plt.rcParams['axes.labelsize'] = 8
plt.rcParams['axes.labelweight'] =  'bold'
plt.rcParams['axes.titlesize'] = 15
plt.rcParams['xtick.labelsize'] = 8
plt.rcParams['ytick.labelsize'] = 7
plt.rcParams['legend.fontsize'] = 8
bar_width=0.1

exps=["ech_baseline",
        "ech_modrian_memory",
        "ech_modrian_memory_xmem",
        "hashtable_baseline",
        "hashtable_modrian_memory",
        "hashtable_modrian_memory_xmem",
        "hashtable_hdc_baseline",
        "hashtable_hdc_modrian_memory",
        "hashtable_hdc_modrian_memory_xmem",
        "oracle_baseline",
        "oracle_modrian_memory",
        "oracle_modrian_memory_xmem",
        "radix_baseline",
        "radix_modrian_memory",
        "radix_modrian_memory_xmem"]
for i in range(15):
    expname=exps[i]
    print("Experiment "+expname+" "+str(i+1)+" : " )
    l1_dcache_metadata = {}
    l2_dcache_metadata = {}
    nuca_metadata = {}

    l1_dcache_security = {}
    l2_dcache_security = {}

    l1_dcache_expressive = {}
    l2_dcache_expressive = {}




    blocks_l1 = (2**int(sys.argv[1]))/64
    blocks_l2 = (2**int(sys.argv[2]))/64
    blocks_nuca = (2**int(sys.argv[3]))/64

    names=[n for n in sorted(os.listdir("results/cache_dram_stats-2")) if (os.path.isdir("results/cache_dram_stats-2/"+n) and ("sls" not in n))]
    for a in range(len(names)):
        if a%15==i:
            print(names[a])
            if(os.path.exists("results/cache_dram_stats-2/"+names[a]+"/l1_dcache_data0")):
                l1_dcache_metadata[names[a]] = 0
                with open("results/cache_dram_stats-2/"+names[a]+"/l1_dcache_data0") as file:
                    lines = file.readlines()
                    for line in lines:
                        l1_dcache_metadata[names[a]]+=((((int)(line)*1.0)/(blocks_l1*1.0))*100)
                l1_dcache_metadata[names[a]]/=len(lines)
                #print(l1_dcache_metadata[names[a]])
            if(os.path.exists("results/cache_dram_stats-2/"+names[a]+"/l2_cache_data0")):
                l2_dcache_metadata[names[a]] = 0
                with open("results/cache_dram_stats-2/"+names[a]+"/l2_cache_data0") as file:
                    lines = file.readlines()
                    for line in lines:
                        l2_dcache_metadata[names[a]]+=((((int)(line)*1.0)/(blocks_l2*1.0))*100)
                l2_dcache_metadata[names[a]]/=len(lines)
                #print(l2_dcache_metadata[names[a]])
            if(os.path.exists("results/cache_dram_stats-2/"+names[a]+"/nuca_ptw_data0")):
                nuca_metadata[names[a]] = 0
                with open("results/cache_dram_stats-2/"+names[a]+"/nuca_ptw_data0") as file:
                    lines = file.readlines()
                    for line in lines:
                        nuca_metadata[names[a]]+=((((int)(line)*1.0)/(blocks_nuca*1.0))*100)
                nuca_metadata[names[a]]/=len(lines)
                #print(nuca_metadata[names[a]])
            if(i%3==1 or i%3==2):
                if(os.path.exists("results/cache_dram_stats-2/"+names[a]+"/l1_dcache_data_security0")):
                    l1_dcache_security[names[a]] = 0
                    with open("results/cache_dram_stats-2/"+names[a]+"/l1_dcache_data_security0") as file:
                        lines = file.readlines()
                        for line in lines:
                            l1_dcache_security[names[a]]+=((((int)(line)*1.0)/(blocks_l1*1.0))*100)
                    l1_dcache_security[names[a]]/=len(lines)
                    #print(l1_dcache_security[names[a]])
                if(os.path.exists("results/cache_dram_stats-2/"+names[a]+"/l2_cache_data_security0")):
                    l2_dcache_security[names[a]] = 0
                    with open("results/cache_dram_stats-2/"+names[a]+"/l2_cache_data_security0") as file:
                        lines = file.readlines()
                        for line in lines:
                            l2_dcache_security[names[a]]+=((((int)(line)*1.0)/(blocks_l2*1.0))*100)
                    l2_dcache_security[names[a]]/=len(lines)
                    #print(l2_dcache_security[names[a]])
            if(i%3==2):
                if(os.path.exists("results/cache_dram_stats-2/"+names[a]+"/l1_dcache_data_expressive0")):
                    l1_dcache_expressive[names[a]] = 0
                    with open("results/cache_dram_stats-2/"+names[a]+"/l1_dcache_data_expressive0") as file:
                        lines = file.readlines()
                        for line in lines:
                            l1_dcache_expressive[names[a]]+=((((int)(line)*1.0)/(blocks_l1*1.0))*100)
                    l1_dcache_expressive[names[a]]/=len(lines)
                    #print(l1_dcache_expressive[names[a]])
                if(os.path.exists("results/cache_dram_stats-2/"+names[a]+"/l2_cache_data_expressive0")):
                    l2_dcache_expressive[names[a]] = 0
                    with open("results/cache_dram_stats-2/"+names[a]+"/l2_cache_data_expressive0") as file:
                        lines = file.readlines()
                        for line in lines:
                            l2_dcache_expressive[names[a]]+=((((int)(line)*1.0)/(blocks_l2*1.0))*100)
                    l2_dcache_expressive[names[a]]/=len(lines)
                    #print(l2_dcache_expressive[names[a]])
    
    print("Plotting l1 metadata")
    app_names = []
    average_occupancy=[]
    for folder in sorted(l1_dcache_metadata):
        app_names.append(folder.split("_")[0])
        average_occupancy.append(l1_dcache_metadata[folder])
    plt.cla()
    plt.clf()
    fig, axs = plt.subplots()
    fig.set_size_inches(6,2)
    axs.bar(np.arange(0,len(l1_dcache_metadata)),average_occupancy,linewidth=1.0,width=0.5,label=app_names)
    plt.xticks(np.arange(0,len(l1_dcache_metadata)),app_names,rotation=40)
    plt.xlabel("Applications", fontsize = 7)
    fig.text(0.06, 0.5, "Ratio of L1 cache blocks with \n Page Table Entries (%)", ha='center', va='center', rotation='vertical',fontsize = 8)
    plt.savefig("./plots/l1_dcache_metadata_"+expname+"_stacked.pdf", bbox_inches="tight",dpi=300)
    plt.savefig("./plots/l1_dcache_metadata_"+expname+"_stacked.png", bbox_inches="tight",dpi=300)

    print("Plotting l2 metadata")
    app_names = []
    average_occupancy=[]
    for folder in sorted(l2_dcache_metadata):
        app_names.append(folder.split("_")[0])
        average_occupancy.append(l2_dcache_metadata[folder])
    plt.cla()
    plt.clf()
    fig, axs = plt.subplots()
    fig.set_size_inches(6,2)
    axs.bar(np.arange(0,len(l2_dcache_metadata)),average_occupancy,linewidth=1.0,width=0.5,label=app_names)
    plt.xticks(np.arange(0,len(l2_dcache_metadata)),app_names,rotation=40)
    plt.xlabel("Applications", fontsize = 7)
    fig.text(0.06, 0.5, "Ratio of L1 cache blocks with \n Page Table Entries (%)", ha='center', va='center', rotation='vertical',fontsize = 8)
    plt.savefig("./plots/l2_dcache_metadata_"+expname+"_stacked.pdf", bbox_inches="tight",dpi=300)
    plt.savefig("./plots/l2_dcache_metadata_"+expname+"_stacked.png", bbox_inches="tight",dpi=300)

    print("Plotting nuca metadata")
    app_names = []
    average_occupancy=[]
    for folder in sorted(nuca_metadata):
        app_names.append(folder.split("_")[0])
        average_occupancy.append(nuca_metadata[folder])
    plt.cla()
    plt.clf()
    fig, axs = plt.subplots()
    fig.set_size_inches(6,2)
    axs.bar(np.arange(0,len(nuca_metadata)),average_occupancy,linewidth=1.0,width=0.5,label=app_names)
    plt.xticks(np.arange(0,len(nuca_metadata)),app_names,rotation=40)
    plt.xlabel("Applications", fontsize = 7)
    fig.text(0.06, 0.5, "Ratio of L1 cache blocks with \n Page Table Entries (%)", ha='center', va='center', rotation='vertical',fontsize = 8)
    plt.savefig("./plots/nuca_metadata_"+expname+"_stacked.pdf", bbox_inches="tight",dpi=300)
    plt.savefig("./plots/nuca_metadata_"+expname+"_stacked.png", bbox_inches="tight",dpi=300)

    if(i%3==1 or i%3==2):
        print("Plotting l1 security")
        app_names = []
        average_occupancy=[]
        for folder in sorted(l1_dcache_security):
            app_names.append(folder.split("_")[0])
            average_occupancy.append(l1_dcache_security[folder])
        plt.cla()
        plt.clf()
        fig, axs = plt.subplots()
        fig.set_size_inches(6,2)
        axs.bar(np.arange(0,len(l1_dcache_security)),average_occupancy,linewidth=1.0,width=0.5,label=app_names)
        plt.xticks(np.arange(0,len(l1_dcache_security)),app_names,rotation=40)
        plt.xlabel("Applications", fontsize = 7)
        fig.text(0.06, 0.5, "Ratio of L1 cache blocks with \n Page Table Entries (%)", ha='center', va='center', rotation='vertical',fontsize = 8)
        plt.savefig("./plots/l1_dcache_security_"+expname+"_stacked.pdf", bbox_inches="tight",dpi=300)
        plt.savefig("./plots/l1_dcache_security_"+expname+"_stacked.png", bbox_inches="tight",dpi=300)

        print("Plotting l2 security")
        app_names = []
        average_occupancy=[]
        for folder in sorted(l2_dcache_security):
            app_names.append(folder.split("_")[0])
            average_occupancy.append(l2_dcache_security[folder])
        plt.cla()
        plt.clf()
        fig, axs = plt.subplots()
        fig.set_size_inches(6,2)
        axs.bar(np.arange(0,len(l2_dcache_security)),average_occupancy,linewidth=1.0,width=0.5,label=app_names)
        plt.xticks(np.arange(0,len(l2_dcache_security)),app_names,rotation=40)
        plt.xlabel("Applications", fontsize = 7)
        fig.text(0.06, 0.5, "Ratio of L1 cache blocks with \n Page Table Entries (%)", ha='center', va='center', rotation='vertical',fontsize = 8)
        plt.savefig("./plots/l2_dcache_security_"+expname+"_stacked.pdf", bbox_inches="tight",dpi=300)
        plt.savefig("./plots/l2_dcache_security_"+expname+"_stacked.png", bbox_inches="tight",dpi=300)
    if(i%3==2):
        print("Plotting l1 expressive")
        app_names = []
        average_occupancy=[]
        for folder in sorted(l1_dcache_expressive):
            app_names.append(folder.split("_")[0])
            average_occupancy.append(l1_dcache_expressive[folder])
        plt.cla()
        plt.clf()
        fig, axs = plt.subplots()
        fig.set_size_inches(6,2)
        axs.bar(np.arange(0,len(l1_dcache_expressive)),average_occupancy,linewidth=1.0,width=0.5,label=app_names)
        plt.xticks(np.arange(0,len(l1_dcache_expressive)),app_names,rotation=40)
        plt.xlabel("Applications", fontsize = 7)
        fig.text(0.06, 0.5, "Ratio of L1 cache blocks with \n Page Table Entries (%)", ha='center', va='center', rotation='vertical',fontsize = 8)
        plt.savefig("./plots/l1_dcache_expressive_"+expname+"_stacked.pdf", bbox_inches="tight",dpi=300)
        plt.savefig("./plots/l1_dcache_expressive_"+expname+"_stacked.png", bbox_inches="tight",dpi=300)

        print("Plotting l2 expressive")
        app_names = []
        average_occupancy=[]
        for folder in sorted(l2_dcache_expressive):
            app_names.append(folder.split("_")[0])
            average_occupancy.append(l2_dcache_expressive[folder])
        plt.cla()
        plt.clf()
        fig, axs = plt.subplots()
        fig.set_size_inches(6,2)
        axs.bar(np.arange(0,len(l2_dcache_expressive)),average_occupancy,linewidth=1.0,width=0.5,label=app_names)
        plt.xticks(np.arange(0,len(l2_dcache_expressive)),app_names,rotation=40)
        plt.xlabel("Applications", fontsize = 7)
        fig.text(0.06, 0.5, "Ratio of L1 cache blocks with \n Page Table Entries (%)", ha='center', va='center', rotation='vertical',fontsize = 8)
        plt.savefig("./plots/l2_dcache_expressive_"+expname+"_stacked.pdf", bbox_inches="tight",dpi=300)
        plt.savefig("./plots/l2_dcache_expressive_"+expname+"_stacked.png", bbox_inches="tight",dpi=300)
    # for folder in sorted(os.listdir("results/cache_dram_stats-2")):
    #     if("sls" not in folder):
    #         if (os.path.isdir("results/cache_dram_stats-2/"+folder)):
    #             if(os.path.exists("results/cache_dram_stats-2/"+folder+"/"+sys.argv[2])):
    #                 nuca_map_ptw[folder] = []
    #                 with open("results/"+sys.argv[1]+"/"+folder+"/"+sys.argv[2]) as nuca_file:
    #                     lines = nuca_file.readlines()
    #                     for line in lines:
    #                         nuca_map_ptw[folder].append((((int)(line)*1.0)/(blocks*1.0))*100)