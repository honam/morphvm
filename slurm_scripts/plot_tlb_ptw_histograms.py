
import os 
import sys
import csv
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import host_subplot
import matplotlib.ticker as ticker
import mpl_toolkits.axisartist as AA
from collections import defaultdict



plt.style.use("grayscale")
plt.rcParams['font.family'] = 'serif'
plt.rcParams['font.serif'] = 'Ubuntu'
plt.rcParams['font.monospace'] = 'Ubuntu Mono'
#plt.rcParams['font.size'] = 10
plt.rcParams['font.weight']='bold'
plt.rcParams['axes.labelsize'] = 9
plt.rcParams['axes.labelweight'] =  'bold'
plt.rcParams['axes.titlesize'] = 10
plt.rcParams['xtick.labelsize'] = 5
plt.rcParams['ytick.labelsize'] = 7
plt.rcParams['legend.fontsize'] = 5
bar_width=0.5





l2tlbfile = sys.argv[1]+"l2_tlb_miss0"
ptwlatencyfile= sys.argv[1]+"ptw_latency_heatmap0"



print l2tlbfile

L2TLB_misses = []
PTW_latencies = []

with open(l2tlbfile) as l2tlb:
        lines = l2tlb.readlines()
        i = 0
        for line in lines:
            if(i > 3):
                address = line.split(" ")[0]
                value = (int)(line.split(" ")[1].strip('\n'))
                L2TLB_misses.append((address,value))
            i = i + 1

       
with open(ptwlatencyfile) as ptwlatency:
        lines = ptwlatency.readlines()
        i = 0
        for line in lines:
            if( i > 3):
                address = line.split(" ")[0]
                value = (int)(line.split(" ")[1].strip('\n'))
                PTW_latencies.append((address,value))
            i = i + 1
       


print len(L2TLB_misses)

L2TLB_misses.sort(key=lambda x:x[1])

sum1 =0
for (key,value) in L2TLB_misses:
        sum1 += value


print L2TLB_misses[0]
total_len =  len(L2TLB_misses)

#L2TLB_misses =  [value for (key, value) in L2TLB_misses if value > 40]
#print((len(L2TLB_misses) - 50000*1.0)/total_len*1.0)

bucket = [0,0,0,0,0]
for (key,value) in L2TLB_misses:
        if(value == 1):
                bucket[0]+=1
        if(value  < 4):
                bucket[1]+=1
        if(value < 8):
                bucket[2]+=1
        if(value < 16):
                bucket[3]+=1
        if(value > 16):
                bucket[4]+=1

print bucket
sum2 =0
for i in range(50000,len(L2TLB_misses)):
        sum2 += L2TLB_misses[i]


print(sum2*1.0/sum1*1.0)


print len(L2TLB_misses)

plt.cla()
plt.clf()
    
fig, axs = plt.subplots(1)
fig.set_size_inches(4,2)

axs.set_ylabel("L2 TLB Misses",fontsize = 7)    
axs.set_xlabel("Virtual Pages",fontsize = 7)

axs.yaxis.grid(color = "grey",linewidth=0.5,zorder=1)
ind  = np.arange(0,len(list(L2TLB_misses)),1)
#axs.axvspan(50000, len(L2TLB_misses), alpha=0.5, color='gray')


axs.plot(ind,L2TLB_misses,alpha=0.9,linewidth=1.0,color="red")
    
plt.subplots_adjust(hspace=0.8)
#axs.text(50000,1250,"0.01% of pages\n0.46% of Page Table Walks ",fontsize=4)
plt.savefig("./l2tlbmiss.png", bbox_inches="tight",dpi=300)






PTW_latencies.sort(key=lambda x:x[1], reverse=True)

sum1 =0
for (key,value) in PTW_latencies:
        sum1 += value


print PTW_latencies[0]
total_len = len(PTW_latencies)

PTW_latencies =  [value for (key, value) in PTW_latencies ]




sum2 = 0
i=0
while(1):
        if(sum2*1.0/sum1*1.0 > 0.79):
                break
        sum2 += PTW_latencies[i]
        i = i + 1


print(sum2*1.0/sum1*1.0)
print((i*1.0)/total_len*1.0)


plt.cla()
plt.clf()
    
fig, axs = plt.subplots(1)
fig.set_size_inches(4,2)

axs.set_ylabel("PTW Latency",fontsize = 7)    
axs.set_xlabel("Virtual Pages",fontsize = 7)
axs.axvspan(i, len(PTW_latencies), alpha=0.5, color='gray')

axs.yaxis.grid(color = "grey",linewidth=0.5,zorder=1)
ind  = np.arange(0,len(list(PTW_latencies)),1)


axs.plot(ind,PTW_latencies,alpha=0.9,linewidth=1.0,color="red")
plt.subplots_adjust(hspace=0.8)
plt.savefig(sys.argv[1]+"./ptwlatency.png", bbox_inches="tight",dpi=300)























