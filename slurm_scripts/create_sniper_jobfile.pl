#!/usr/bin/perl

use lib '/home/honam/simulators/sniper/slurm_scripts/';
use warnings;
use Getopt::Long;
use Trace;
use Exp;

# defaults
my $SNIPER_ROOT="/home/honam/simulators/sniper/";
my $tlist_file;
my $exp_file;
my $exe;
my $local = "0";
my $ncores;
my $exclude_list;
my $include_list;
my $extra;

GetOptions('tlist=s' => \$tlist_file,
	   'exp=s' => \$exp_file,
	   'exe=s' => \$exe,
	   'ncores=s' => \$ncores,
	   'local=s' => \$local,
	   'exclude=s' => \$exclude_list,
	   'include=s' => \$include_list,
	   'extra=s' => \$extra,
) or die "Usage: $0 --exe <executable> --exp <exp file> --tlist <trace list>\n";

die "Supply exe\n" unless defined $exe;
die "Supply tlist\n" unless defined $tlist_file;
die "Supply exp\n" unless defined $exp_file;
die "Supply ncores\n" unless defined $ncores;

my $exclude_nodes_list = "";
$exclude_nodes_list = "kratos[$exclude_list]" if defined $exclude_list;
my $include_nodes_list = "";
$include_nodes_list = "kratos[$include_list]" if defined $include_list;

my @trace_info = Trace::parse($tlist_file);
my @exp_info = Exp::parse($exp_file);

# preamble for sbatch script
if($local eq "0")
{
	print "#!/bin/bash -l\n";
	print "#\n";
	print "#\n";
}

print "#\n";
print "# Traces:\n";
foreach $trace (@trace_info)
{
	my $trace_name = $trace->{"NAME"};
	print "#    $trace_name\n";
}
print "#\n";

print "#\n";
print "# Experiments:\n";
foreach $exp (@exp_info)
{
	my $exp_name = $exp->{"NAME"};
	my $exp_knobs = $exp->{"KNOBS"};
	print "#    $exp_name: $exp_knobs\n";
}
print "#\n";
print "#\n";
print "#\n";
print "#\n";

foreach $trace (@trace_info)
{
	foreach $exp (@exp_info)
	{
		my $exp_name = $exp->{"NAME"};
		my $exp_knobs = $exp->{"KNOBS"};
		my $trace_name = $trace->{"NAME"};
		# my $trace_exe = $trace->{"EXE"};
		my $trace_input = $trace->{"TRACE"};
		my $trace_knobs = $trace->{"KNOBS"};

		my $cmdline;
		if($local)
		{
			$cmdline = "$exe \"$exp_knobs --traces=$trace_input $trace_knobs\"";
		}
		else
		{
			$slurm_cmd = "sbatch -p slurm_part --mincpus=1";
			if (defined $include_list)
			{
				$slurm_cmd = $slurm_cmd." --nodelist=${include_nodes_list}";
			}
			if (defined $exclude_list)
			{
				$slurm_cmd = $slurm_cmd." --exclude=${exclude_nodes_list}";
			}
			if (defined $extra)
			{
				$slurm_cmd = $slurm_cmd." $extra";
			}
			$slurm_cmd = $slurm_cmd." -c $ncores -J ${trace_name}_${exp_name} -o ${trace_name}_${exp_name}.out -e ${trace_name}_${exp_name}.err";
			if($trace_knobs eq ""){
				$cmdline = "$slurm_cmd $exe \"$exp_knobs --traces=$trace_input $trace_knobs\"";
			}
			else{
				$cmdline = "$slurm_cmd $exe \"$exp_knobs --roi --no-cache-warming -- $trace_input $trace_knobs\"";
			}
		}

		# Additional hook replace
		$cmdline =~ s/\$\(EXP\)/$exp_name/g;
		$cmdline =~ s/\$\(TRACE\)/$trace_name/g;
		$cmdline =~ s/\$\(NCORES\)/$ncores/g;
		
		print "$cmdline\n";
	}
}