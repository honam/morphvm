
import os 
import sys
import csv
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import host_subplot
import matplotlib.ticker as ticker
import mpl_toolkits.axisartist as AA
from collections import defaultdict



plt.style.use("default")
plt.rcParams['font.family'] = 'sans-serif'
plt.rcParams['font.serif'] = 'Ubuntu'
plt.rcParams['font.monospace'] = 'Ubuntu Mono'
#plt.rcParams['font.size'] = 10
plt.rcParams['font.weight']='bold'
plt.rcParams['axes.labelsize'] = 8
plt.rcParams['axes.labelweight'] =  'bold'
plt.rcParams['axes.titlesize'] = 15
plt.rcParams['xtick.labelsize'] = 8
plt.rcParams['ytick.labelsize'] = 7
plt.rcParams['legend.fontsize'] = 8
bar_width=0.5





nuca_map_ptw = {}
nuca_map_utopia = {}

blocks = 8*(2**20)/64
for folder in sorted(os.listdir(sys.argv[1])):
        if (os.path.isdir(sys.argv[1]+"/"+folder)):
                if("radix" in folder):
                        print folder
                        nuca_map_ptw[folder] = []
                        with open(sys.argv[1]+"/"+folder+"/nuca_ptw_data0") as nuca_file:
                                lines = nuca_file.readlines()
                                for line in lines:
                                        nuca_map_ptw[folder].append(((int)(line)*1.0)/(blocks*1.0)*100)

for folder in sorted(os.listdir(sys.argv[2])):
        if (os.path.isdir(sys.argv[2]+"/"+folder)):
                if("pfmig_pte" in folder):
                        print folder
                        nuca_map_utopia[folder] = []
                        with open(sys.argv[2]+"/"+folder+"/nuca_utopia_data0") as nuca_file:
                                lines = nuca_file.readlines()
                                for line in lines:
                                        nuca_map_utopia[folder].append(((int)(line)*1.0)/(blocks*1.0)*100)
                        
                        

plt.cla()
plt.clf()
    
fig, axs = plt.subplots(3, sharex=True)
fig.set_size_inches(6,3)

plt.xticks([0,100000,200000,300000,400000,500000,600000,700000,800000], 
           ["0", "100K", "200K", "300K", "400K", "500K","600K","700K","800K"])

#plt.ylabel("Ratio of LLC cache blocks with \n Translation Table Entries (%)",fontsize = 8)    
plt.xlabel("Epochs (1K instructions interval)",fontsize = 7)
fig.text(0.06, 0.5, "Ratio of LLC cache blocks with \n Translation Table Entries (%)", ha='center', va='center', rotation='vertical',fontsize = 8)

i=1

radix = "Baseline"
utopia_pf_pte = "Utopia-PF-PTE"

pagerank= "PR"
sssp= "SSSP"
bc= "BC"
bfs = "BFS"
tc = "TC"

for folder in sorted(nuca_map_ptw):
        if(folder.split("_")[-1] == "radix"):

                if (folder.split("_")[-1] == "radix"):
                        string=radix
                
                if(folder.split('_')[0] == "pagerank" or  folder.split('_')[0] == "bfs" or folder.split('_')[0] == "tc"):
                        if(folder.split('_')[0]=="pagerank"):
                                label =  pagerank+"_"+string
                                ax = axs[0]
                        if(folder.split('_')[0]=="bfs"):
                                label = bfs+"_"+string
                                ax = axs[1]
                        if(folder.split('_')[0]=="tc"):
                                label = tc+"_"+string
                                ax = axs[2]
                        print len(nuca_map_ptw[folder])
                        ind  = np.arange(0,len(nuca_map_ptw[folder]),1)
                        ax.plot(ind,nuca_map_ptw[folder],linewidth=1.0 ,label=label)
                        i = i + 1

for folder in sorted(nuca_map_utopia):
        if("pfmig_pte" in folder and not("pfmig_pte_cuckoo" in folder) ):
                string=utopia_pf_pte
                
                if(folder.split('_')[0] == "pagerank" or  folder.split('_')[0] == "bfs" or folder.split('_')[0] == "tc"):
                        if(folder.split('_')[0]=="pagerank"):
                                label =   pagerank+"_"+string
                                ax = axs[0]
                        if(folder.split('_')[0]=="bfs"):
                                label =   bfs+"_"+string
                                ax = axs[1]
                        if(folder.split('_')[0]=="tc"):
                                label =   tc+"_"+string
                                ax = axs[2]
                        print len(nuca_map_utopia[folder])
                        ind  = np.arange(0,len(nuca_map_utopia[folder]),1)
                        ax.plot(ind,nuca_map_utopia[folder],linewidth=1.0 ,label=label)
                        i = i + 1
for ax in axs:
    ax.legend(loc="lower center", ncol =2, fontsize=6, bbox_to_anchor=(0.58,0.9))
# axs[0].legend(loc="lower center", ncol =2, fontsize=6, bbox_to_anchor=(0.58,0.9))
# axs[1].legend(loc="lower center", ncol =2, fontsize=6, bbox_to_anchor=(0.58,0.9))
# axs[2].legend(loc="lower center", ncol =2, fontsize=6, bbox_to_anchor=(0.58,0.9))

plt.subplots_adjust(hspace=0.5)
plt.savefig("./nuca_data.pdf", bbox_inches="tight",dpi=300)
plt.savefig("./nuca_data.png", bbox_inches="tight",dpi=300)



















