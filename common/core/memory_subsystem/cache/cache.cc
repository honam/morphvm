#include "simulator.h"
#include "cache.h"
#include "log.h"
#include "stats.h"

// Cache class
// constructors/destructors
Cache::Cache(
   String name,
   String cfgname,
   core_id_t core_id,
   UInt32 num_sets,
   UInt32 associativity,
   UInt32 cache_block_size,
   String replacement_policy,
   cache_t cache_type,
   hash_t hash,
   FaultInjector *fault_injector,
   AddressHomeLookup *ahl,
   bool is_tlb, int* page_size, int number_of_page_sizes)
:
   CacheBase(name, num_sets, associativity, cache_block_size, hash, ahl),
   m_enabled(false),
   m_num_accesses(0),
   m_num_hits(0),
   m_cache_type(cache_type),
   m_fault_injector(fault_injector),
   m_number_of_page_sizes(number_of_page_sizes),
   m_pagesizes(page_size),
   sum_data_reuse(0),
   number_of_data_reuse(0),
   sum_metadata_reuse(0),
   number_of_metadata_reuse(0)
{
   reuse_levels[0]=5;
   reuse_levels[1]=10;
   reuse_levels[2]=20;

   m_set_info = CacheSet::createCacheSetInfo(name, cfgname, core_id, replacement_policy, m_associativity);
   m_sets = new CacheSet*[m_num_sets];
   for (UInt32 i = 0; i < m_num_sets; i++)
   {
      m_sets[i] = CacheSet::createCacheSet(cfgname, core_id, replacement_policy, m_cache_type, m_associativity, m_blocksize, m_set_info);
   }

   #ifdef ENABLE_SET_USAGE_HIST
   m_set_usage_hist = new UInt64[m_num_sets];
   for (UInt32 i = 0; i < m_num_sets; i++)
      m_set_usage_hist[i] = 0;
   #endif

   m_page_walk_cacheblocks.clear();
   m_is_tlb = true;

   for (int i=0; i < 5; i++){
         
         registerStatsMetric(name, core_id, String("data-reuse-")+std::to_string(i).c_str(), &data_reuse[i]);
   }
   for (int i=0; i < 5; i++){
         
         registerStatsMetric(name, core_id, String("metadata-reuse-")+std::to_string(i).c_str(), &metadata_reuse[i]);
   }
   registerStatsMetric(name, core_id, String("average_data_reuse"), &average_data_reuse);
   registerStatsMetric(name, core_id, String("average_metadata_reuse"), &average_metadata_reuse);
}

Cache::~Cache()
{

   #ifdef ENABLE_SET_USAGE_HIST
   printf("Cache %s set usage:", m_name.c_str());
   for (SInt32 i = 0; i < (SInt32) m_num_sets; i++)
      printf(" %" PRId64, m_set_usage_hist[i]);
   printf("\n");
   delete [] m_set_usage_hist;
   #endif

   if (m_set_info)
      delete m_set_info;

   for (SInt32 i = 0; i < (SInt32) m_num_sets; i++)
      delete m_sets[i];
   delete [] m_sets;
}

Lock&
Cache::getSetLock(IntPtr addr)
{
   IntPtr tag;
   UInt32 set_index;

   splitAddress(addr, tag, set_index);
   assert(set_index < m_num_sets);

   return m_sets[set_index]->getLock();
}

bool
Cache::invalidateSingleLine(IntPtr addr)
{
   IntPtr tag;
   UInt32 set_index;

   splitAddress(addr, tag, set_index);
   assert(set_index < m_num_sets);

   return m_sets[set_index]->invalidate(tag);
}

CacheBlockInfo*
Cache::accessSingleLine(IntPtr addr, access_t access_type,
      Byte* buff, UInt32 bytes, SubsecondTime now, bool update_replacement)
{
   //assert((buff == NULL) == (bytes == 0));

   IntPtr tag;
   UInt32 set_index;
   UInt32 line_index = -1;
   UInt32 block_offset;
   splitAddress(addr, tag, set_index, block_offset);

   CacheSet* set = m_sets[set_index];
   CacheBlockInfo* cache_block_info = set->find(tag, &line_index);

   if (cache_block_info == NULL)
      return NULL;

   if (access_type == LOAD)
   {
      // NOTE: assumes error occurs in memory. If we want to model bus errors, insert the error into buff instead
      if (m_fault_injector)
         m_fault_injector->preRead(addr, set_index * m_associativity + line_index, bytes, (Byte*)m_sets[set_index]->getDataPtr(line_index, block_offset), now);

      set->read_line(line_index, block_offset, buff, bytes, update_replacement);
   }
   else
   {
      set->write_line(line_index, block_offset, buff, bytes, update_replacement);

      // NOTE: assumes error occurs in memory. If we want to model bus errors, insert the error into buff instead
      if (m_fault_injector)
         m_fault_injector->postWrite(addr, set_index * m_associativity + line_index, bytes, (Byte*)m_sets[set_index]->getDataPtr(line_index, block_offset), now);
   }

   return cache_block_info;
}

void
Cache::insertSingleLine(IntPtr addr, Byte* fill_buff,
      bool* eviction, IntPtr* evict_addr,
      CacheBlockInfo* evict_block_info, Byte* evict_buff,
      SubsecondTime now, CacheCntlr *cntlr,
      CacheBlockInfo::block_type_t btype)
{
   IntPtr tag;
   UInt32 set_index;
   splitAddress(addr, tag, set_index);

   CacheBlockInfo* cache_block_info = CacheBlockInfo::create(m_cache_type);
   cache_block_info->setTag(tag);
   cache_block_info->setBlockType(btype);

   m_sets[set_index]->insert(cache_block_info, fill_buff,
         eviction, evict_block_info, evict_buff, cntlr);
   *evict_addr = tagToAddress(evict_block_info->getTag());

   if((*eviction) == true){
     // std::cout<<"Eviction\n";
      int reuse_value = evict_block_info->getReuse();
     // std::cout << "Evicting block with address: (data) " << tagToAddress(evict_block_info->getTag()) << " and reuse value: " << reuse_value << std::endl;
      if(cache_block_info->getBlockType()==CacheBlockInfo::block_type_t::NON_PAGE_TABLE){

         if(reuse_value == 0) data_reuse[0]++;
         else if(reuse_value <= reuse_levels[0] ) data_reuse[1]++;
         else if(reuse_value <= reuse_levels[1]) data_reuse[2]++;
         else if(reuse_value <= reuse_levels[2]) data_reuse[3]++;
         else data_reuse[4]++;

         sum_data_reuse+=reuse_value;
         number_of_data_reuse++;
         average_data_reuse = sum_data_reuse/number_of_data_reuse;
         //std::cout << "Average data reuse: " << average_data_reuse << std::endl;

      }
      else if(cache_block_info->getBlockType()==CacheBlockInfo::block_type_t::PAGE_TABLE){
      //   std::cout << "Evicting block with address (metadata): " << tagToAddress(evict_block_info->getTag()) << " and reuse value: " << reuse_value << std::endl;
         if(reuse_value == 0) metadata_reuse[0]++;
         else if(reuse_value <= reuse_levels[0] ) metadata_reuse[1]++;
         else if(reuse_value <= reuse_levels[1]) metadata_reuse[2]++;
         else if(reuse_value <= reuse_levels[2]) metadata_reuse[3]++;
         else metadata_reuse[4]++;

         sum_metadata_reuse+=reuse_value;
         number_of_metadata_reuse++;
         average_metadata_reuse = sum_metadata_reuse/number_of_metadata_reuse;
         //std::cout << "Average metadata reuse: " << average_metadata_reuse << std::endl;
      }
      
   }

   if (m_fault_injector) {
      // NOTE: no callback is generated for read of evicted data
      UInt32 line_index = -1;
      __attribute__((unused)) CacheBlockInfo* res = m_sets[set_index]->find(tag, &line_index);
      LOG_ASSERT_ERROR(res != NULL, "Inserted line no longer there?");

      m_fault_injector->postWrite(addr, set_index * m_associativity + line_index, m_sets[set_index]->getBlockSize(), (Byte*)m_sets[set_index]->getDataPtr(line_index, 0), now);
   }

   #ifdef ENABLE_SET_USAGE_HIST
   ++m_set_usage_hist[set_index];
   #endif

   delete cache_block_info;
}



CacheBlockInfo*
Cache::accessSingleLineTLB(IntPtr addr, access_t access_type,
      Byte* buff, UInt32 bytes, SubsecondTime now, bool update_replacement)
{
   IntPtr tag;
   UInt32 set_index;
   UInt32 line_index = -1;
   UInt32 block_offset;
   CacheBlockInfo* cache_block_info;
   CacheSet* set;
   bool found_cache_block = false;
   
   
   for(int page_size = 0; page_size < m_number_of_page_sizes; page_size++){ // @kanellok iterate over all possible page sizes

      // std::cout << m_name << " lookup: Page size = " << m_pagesizes[page_size] << " Number of page sizes = " <<  m_number_of_page_sizes <<  std::endl;
       splitAddressTLB(addr, tag, set_index, block_offset, m_pagesizes[page_size]); //@kanellok provide the page size to find the index
      // std::cout << "Address =  " << addr << std::endl;
      // std::cout << "Set index =  " << set_index << std::endl;
      // std::cout << "Tag =  " << tag  << std::endl;
      // std::cout << std::endl;



      set = m_sets[set_index];
      cache_block_info = set->find(tag, &line_index);

      //std::cout << "Set - Cache Block Info " << set->find(tag, &line_index) << std::endl;

      if (cache_block_info == NULL)
         continue;

      found_cache_block = true; 

      if (access_type == LOAD)
      {
         // NOTE: assumes error occurs in memory. If we want to model bus errors, insert the error into buff instead
         if (m_fault_injector)
            m_fault_injector->preRead(addr, set_index * m_associativity + line_index, bytes, (Byte*)m_sets[set_index]->getDataPtr(line_index, block_offset), now);

         set->read_line(line_index, block_offset, buff, bytes, update_replacement);
      }
      else
      {
         set->write_line(line_index, block_offset, buff, bytes, update_replacement);

         // NOTE: assumes error occurs in memory. If we want to model bus errors, insert the error into buff instead
         if (m_fault_injector)
            m_fault_injector->postWrite(addr, set_index * m_associativity + line_index, bytes, (Byte*)m_sets[set_index]->getDataPtr(line_index, block_offset), now);
      }
      break;
   }

   //std::cout << "Outcome = " << (bool)(cache_block_info) << std::endl;
   //std::cout << std::endl;
   

   if(found_cache_block) return cache_block_info;
   else return NULL;
}

void
Cache::insertSingleLineTLB(IntPtr addr, Byte* fill_buff,
      bool* eviction, IntPtr* evict_addr,
      CacheBlockInfo* evict_block_info, Byte* evict_buff,
      SubsecondTime now, CacheCntlr *cntlr,
      CacheBlockInfo::block_type_t btype, int page_size)
{
    IntPtr tag;
   UInt32 set_index;
   UInt32 line_index = -1;
   splitAddressTLB(addr, tag, set_index, page_size);

      // std::cout << m_name << " insert: Page size = " << page_size << std::endl;
      // std::cout << "Address =  " << addr << std::endl;
      // std::cout << "Set index =  " << set_index << std::endl;
      // std::cout << "Tag =  " << tag  << std::endl;
      // std::cout << std::endl;


   CacheBlockInfo* cache_block_info = CacheBlockInfo::create(m_cache_type);
   cache_block_info->setTag(tag);
   cache_block_info->setBlockType(btype);

   m_sets[set_index]->insert(cache_block_info, fill_buff,
         eviction, evict_block_info, evict_buff, cntlr);
   *evict_addr = tagToAddress(evict_block_info->getTag());


  // std::cout << "Inserted " << addr << " in set " << set_index << " with tag " << m_sets[set_index]->find(tag,&line_index) << std::endl;

   if (m_fault_injector) {
      // NOTE: no callback is generated for read of evicted data
      UInt32 line_index = -1;
      __attribute__((unused)) CacheBlockInfo* res = m_sets[set_index]->find(tag, &line_index);
      LOG_ASSERT_ERROR(res != NULL, "Inserted line no longer there?");

      m_fault_injector->postWrite(addr, set_index * m_associativity + line_index, m_sets[set_index]->getBlockSize(), (Byte*)m_sets[set_index]->getDataPtr(line_index, 0), now);
   }

   #ifdef ENABLE_SET_USAGE_HIST
   ++m_set_usage_hist[set_index];
   #endif

   delete cache_block_info;
}



// Single line cache access at addr
CacheBlockInfo*
Cache::peekSingleLine(IntPtr addr)
{
   IntPtr tag;
   UInt32 set_index;
   splitAddress(addr, tag, set_index);

   return m_sets[set_index]->find(tag);
}

void
Cache::updateCounters(bool cache_hit)
{
   if (m_enabled)
   {
      m_num_accesses ++;
      if (cache_hit)
         m_num_hits ++;
   }
}

void
Cache::updateHits(Core::mem_op_t mem_op_type, UInt64 hits)
{
   if (m_enabled)
   {
      m_num_accesses += hits;
      m_num_hits += hits;
   }
}

CacheSet* 
Cache::getCacheSet(UInt32 set_index)
{
   return m_sets[set_index];
}

void
Cache::measureStats()
{
   uint64_t accum = 0; /* accumulate stats over all sets */
   for(uint32_t set_index = 0; set_index < m_num_sets; ++set_index)
   {
      accum += m_sets[set_index]->countPageWalkCacheBlocks();
   }

   m_page_walk_cacheblocks.push_back(accum);

   accum = 0; /* accumulate stats over all sets */
   for(uint32_t set_index = 0; set_index < m_num_sets; ++set_index)
   {
      accum += m_sets[set_index]->countSecurityCacheBlocks();
   }

   m_security_cacheblocks.push_back(accum);

   accum = 0; /* accumulate stats over all sets */
   for(uint32_t set_index = 0; set_index < m_num_sets; ++set_index)
   {
      accum += m_sets[set_index]->countExpressiveCacheBlocks();
   }

   m_expressive_cacheblocks.push_back(accum);

   accum = 0; /* accumulate stats over all sets */
   for(uint32_t set_index = 0; set_index < m_num_sets; ++set_index)
   {
      accum += m_sets[set_index]->countUtopiaCacheBlocks();
   }
   m_utopia_cacheblocks.push_back(accum);

}
void Cache::markMetadata(IntPtr address, CacheBlockInfo::block_type_t blocktype){

   
   IntPtr tag;
   UInt32 set_index;

   splitAddress(address, tag, set_index);


   for (int i=0; i < getCacheSet(set_index)->getAssociativity(); i++){
            
      if(peekBlock(set_index,i)->getTag() == tag){
         peekBlock(set_index,i)->setBlockType(blocktype);
         break;
      }
                 
   }

}