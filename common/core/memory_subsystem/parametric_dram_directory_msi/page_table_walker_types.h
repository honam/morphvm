#ifndef PTWT_H
#define PTWT_H

#include <iostream>
#include "pagetable_walker.h"

using namespace ParametricDramDirectoryMSI;

namespace PtwTypes{


    enum ptw_table_entry_type{
        PTW_ADDRESS,
        PTW_TABLE_POINTER,
        PTW_NONE,
        PTW_NUMBER_OF_ENTRY_TYPES
    };

    enum verification_type{
        VERIFY_MT=0,
        VERIFY_NONE
    };

    enum mt_verification_granularity{
        PTE_LEVEL=0,
        PT_LEVEL,
        CL_LEVEL
    };

    enum mt_verification_level{
        LEVEL1=0,
        LEVEL2,
        LEVEL3,
        LEVEL4 // The final PTE
    };
    /** track the level of integrity where
     * L1 -> verify when L1 hit (or other following hits)
     * L2 -> verify when L2 hit, L1 miss (or other following hits)
     * L3 -> verify when L3 hit, L1, L2 miss (or other following hits)
     * MEM -> verify only when memory access (or other following hits)
     */
    enum mt_integrity_level{
        L1=0,
        L2,
        L3,
        MEM
    };


    struct verify_struct{
        verification_type verify_typ;
        mt_verification_granularity mt_verify_gran;
        mt_integrity_level mt_int_level;
        uint16_t mt_node_size; // the number of bits per MT node
        mt_verification_level mt_verify_level; // verification level for page table
        uint16_t cl_size = 64; // cache line size -> for us fixed to 64B
        uint16_t addr_len = 64; // the length of the address
        uint16_t table_size = 512; // the size of the page table
        uint16_t n_partition; // the number of partitions per hash value = the number of nodes in total
        uint16_t n_subpartition; //
        uint64_t total_size;
        uint16_t total_entry;
        uint16_t n_data_partition; // the number of data partition in one entry (given that node adress length is smaller than 64)
        uint16_t n_data_blocks;
        uint16_t n_cl_blocks;
    };

    struct ptw_table_entry {
        ptw_table_entry_type entry_type;
        mt_t* mt_pte; //merkle tree for page table entry level
        struct ptw_table* next_level_table;
        UInt64 phy_addr;
        verify_struct verify_metadata; // meta-data for verification
    };

    struct ptw_table{
        struct ptw_table_entry* entries; //4kB => 512 * 8B => entries[0] , entries[1], entries[3] ,... entries[512-1]
        int table_size;
        mt_t* mt_pt; //merkle tree for page tabel level verification
        mt_t** mt_cl; // merkle tree for cache-line level verification
        verify_struct verify_metadata; // meta-data for verification
    };

    verify_struct* InitiateVerifyMetadata(verification_type verify_typ, mt_verification_granularity mt_verify_gran,
                                          mt_integrity_level mt_int_level, uint16_t mt_node_size, mt_verification_level mt_verify_level);
    ptw_table_entry* CreateNewPtwEntryAtLevel(int level,int number_of_levels,int *level_indices,int *level_percentages,PageTableWalker *ptw, verify_struct *verify_metadata);
    ptw_table* InitiateTablePtw(int size, verify_struct* verify_metadata);
    uint64_t extract(uint64_t value, uint16_t begin, uint16_t end);
    // void HashValue(UInt64 data, uint8_t **hash_values, uint16_t n_partition, uint16_t mt_node_size);
    void HashValue(UInt64 data, uint8_t **hash_values, verify_struct* verify_metadata); //compact version -> we will exclusively use this!
    mt_error_t InitiateMT(mt_t* mt, UInt64* data, verify_struct* verify_metadata);
    void DataToByte(UInt64* data_partition, uint8_t* tags, uint16_t n_subpartition, uint16_t mt_node_size);
    bool verify_mt(mt_t *t, UInt64* data, verify_struct* verify_metadata, uint64_t *addrs, uint8_t *level, uint32_t offset);
    void PrintHash(uint8_t* tag);
    SubsecondTime GetMTLatency(mt_t *mt, int cycles );
    SubsecondTime GetVerifyLatency(mt_t *mt, int cycles);
    // extract the bits in [begin, end)
}

#endif 