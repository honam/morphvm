
#include "cache_cntlr.h"
#include "pwc.h"
#include "subsecond_time.h"
#include "config.hpp"
#include "simulator.h"
#include "memory_manager.h"
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <math.h>
#include <fstream>
#include "PageTableHashDontCache.h"
namespace ParametricDramDirectoryMSI{
    PageTableHashDontCache::PageTableHashDontCache(int _table_size_in_bits,int _small_page_size_in_bits,int _big_page_size_in_bits,int _small_page_percentage,
        Core* _core,ShmemPerfModel* _m_shmem_perf_model,PWC* pwc, bool pwc_enabled)
    :PageTableWalker(_core->getId(), 0, _m_shmem_perf_model, pwc, pwc_enabled),latency_for_disk_access(NULL,0){
        latency_for_disk_access= ComponentLatency(_core->getDvfsDomain(), Sim()->getCfg()->getInt("perf_model/hash_dont_cache/disk_latency"));

        this->table_size_in_bits=_table_size_in_bits;
        this->small_page_size_in_bits=_small_page_size_in_bits;
        this->big_page_size_in_bits=_big_page_size_in_bits;
        this->small_page_percentage=_small_page_percentage;
        this->core=_core;
        this->m_shmem_perf_model=_m_shmem_perf_model;
        
        this->table_small_pages=(entry *)malloc((UInt64)pow(2,table_size_in_bits)*sizeof(entry));
        for (UInt64 i = 0; i < (UInt64)pow(2,table_size_in_bits); i++)
        {
            this->table_small_pages[i].empty=true;
            this->table_small_pages[i].vpn=0;
            this->table_small_pages[i].ptes[0]=NULL;
            this->table_small_pages[i].ptes[1]=NULL;
            this->table_small_pages[i].ptes[2]=NULL;
            this->table_small_pages[i].ptes[3]=NULL;
        }

        this->table_big_pages=(entry *)malloc((UInt64)pow(2,table_size_in_bits)*sizeof(entry));
        for (UInt64 i = 0; i < (UInt64)pow(2,table_size_in_bits); i++)
        {
            this->table_big_pages[i].empty=true;
            this->table_big_pages[i].vpn=0;
            this->table_big_pages[i].ptes[0]=NULL;
            this->table_big_pages[i].ptes[1]=NULL;
            this->table_big_pages[i].ptes[2]=NULL;
            this->table_big_pages[i].ptes[3]=NULL;
        }
    
    
    }

    int PageTableHashDontCache::hash_function(IntPtr address){
        uint64 mask=0;
        for(int i=0 ;i<table_size_in_bits;i++){
            mask+=(int)pow(2,i);
        }
        char*  new_address = (char*) address;
        uint64 result=CityHash64((const char*)&address,8) & (mask);
        //std::cout<<std::hex<<address<<" "<<std::hex<<result<<" "<<std::hex<<mask<<"\n";
        return result;
    }

    SubsecondTime PageTableHashDontCache::init_walk(IntPtr address, UtopiaCache* shadow_cache,
    CacheCntlr *cache, Core::lock_signal_t lock_signal, Byte* data_buf, 
    UInt32 data_length, bool modeled, bool count){
        
        SubsecondTime delay=SubsecondTime::Zero();
        SubsecondTime start=getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
        int initial_hash_function_result=hash_function(address);
        int hash_function_result=initial_hash_function_result;
        int small_pages_page_fault_address=hash_function_result;
        int big_pages_page_fault_address=hash_function_result;
        SubsecondTime access_delay=SubsecondTime::Zero();

        //std::cout<<"Address function: "<<address<<"\n";
        //Code I am bored to make a function
        SubsecondTime t_start = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD); 
        IntPtr cache_address = ((IntPtr)(&table_small_pages[hash_function_result])) & (~((64 - 1))); 
            cache->processMemOpFromCore(
                lock_signal,
                Core::mem_op_t::READ,
                cache_address, 0,
                data_buf, data_length,
                modeled,
                count, CacheBlockInfo::block_type_t::PAGE_TABLE, SubsecondTime::Zero());
        SubsecondTime t_end = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
        if(nuca){
            nuca->markTranslationMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
            mem_manager->getCache(MemComponent::component_t::L1_DCACHE)->markMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
            mem_manager->getCache(MemComponent::component_t::L2_CACHE)->markMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
        }
        getShmemPerfModel()->setElapsedTime(ShmemPerfModel::_USER_THREAD,t_start);
        SubsecondTime t_start_1 = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD); 
        cache_address = ((IntPtr)(&table_big_pages[hash_function_result])) & (~((64 - 1))); 
            cache->processMemOpFromCore(
                lock_signal,
                Core::mem_op_t::READ,
                cache_address, 0,
                data_buf, data_length,
                modeled,
                count, CacheBlockInfo::block_type_t::PAGE_TABLE, SubsecondTime::Zero());
        SubsecondTime t_end_1 = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
        delay += std::max(t_end-t_start,t_end_1-t_start_1) ;
        getShmemPerfModel()->setElapsedTime(ShmemPerfModel::_USER_THREAD,t_start);
        getShmemPerfModel()->incrElapsedTime(std::max(t_end-t_start,t_end_1-t_start_1),ShmemPerfModel::_USER_THREAD);
        if(nuca){
            nuca->markTranslationMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
            mem_manager->getCache(MemComponent::component_t::L1_DCACHE)->markMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
            mem_manager->getCache(MemComponent::component_t::L2_CACHE)->markMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
        }
        //Code I am bored to make a function
        while(!table_small_pages[hash_function_result].empty || !table_big_pages[hash_function_result].empty){
            if(table_small_pages[hash_function_result].empty && small_pages_page_fault_address==initial_hash_function_result){
                small_pages_page_fault_address=hash_function_result;
            }
            else if(table_big_pages[hash_function_result].empty && big_pages_page_fault_address==initial_hash_function_result){
                big_pages_page_fault_address=hash_function_result;
            }
            //std::cout<<"In while loop"<<address<<" "<<hash_function_result<<"\n";
            //std::cout<<table_small_pages[hash_function_result].vpn<<" == "<<(address>>(small_page_size_in_bits+2));
            //std::cout<<table_big_pages[hash_function_result].vpn<<" == "<<(address>>(big_page_size_in_bits+2));
            if(table_small_pages[hash_function_result].vpn == (address>>(small_page_size_in_bits+2))){
                //Code I am bored to make a function
                SubsecondTime t_start = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);                
                IntPtr cache_address = ((IntPtr)(&table_small_pages[hash_function_result].ptes[(address>>small_page_size_in_bits)%4][address%(int)pow(2,small_page_size_in_bits)])) & (~((64 - 1))); 
                    cache->processMemOpFromCore(
                        lock_signal,
                        Core::mem_op_t::READ,
                        cache_address, 0,
                        data_buf, data_length,
                        modeled,
                        count, CacheBlockInfo::block_type_t::PAGE_TABLE, SubsecondTime::Zero());
                SubsecondTime t_end = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
                delay += t_end - t_start ;
                //Code I am bored to make a function
                getShmemPerfModel()->setElapsedTime(ShmemPerfModel::_USER_THREAD,start);
                if(nuca){
                    nuca->markTranslationMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
                    mem_manager->getCache(MemComponent::component_t::L1_DCACHE)->markMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
                    mem_manager->getCache(MemComponent::component_t::L2_CACHE)->markMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
                }
                //std::cout<<"Found in small pages table!\n";
                UInt64 vpn = address >> init_walk_functional(address);
                track_per_page_ptw_latency(vpn,delay);
                //std::cout<<"Hit in small table\n";
                return delay;  
            }
            else if(table_big_pages[hash_function_result].vpn == (address>>(big_page_size_in_bits+2))){
                //Code I am bored to make a function
                SubsecondTime t_start = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);                
                IntPtr cache_address = ((IntPtr)(&table_big_pages[hash_function_result].ptes[(address>>big_page_size_in_bits)%4][address%(int)pow(2,big_page_size_in_bits)])) & (~((64 - 1))); 
                    cache->processMemOpFromCore(
                        lock_signal,
                        Core::mem_op_t::READ,
                        cache_address, 0,
                        data_buf, data_length,
                        modeled,
                        count, CacheBlockInfo::block_type_t::PAGE_TABLE, SubsecondTime::Zero());
                SubsecondTime t_end = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
                delay += t_end - t_start ;
                //Code I am bored to make a function
                getShmemPerfModel()->setElapsedTime(ShmemPerfModel::_USER_THREAD,start);
                if(nuca){
                    nuca->markTranslationMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
                    mem_manager->getCache(MemComponent::component_t::L1_DCACHE)->markMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
                    mem_manager->getCache(MemComponent::component_t::L2_CACHE)->markMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
                }
                //std::cout<<"Found in big pages table!\n";
                UInt64 vpn = address >> init_walk_functional(address);
                track_per_page_ptw_latency(vpn,delay);
                //std::cout<<"Hit in large table\n";
                return delay;  
            }
            else{
                t_start = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD); 
                cache_address = ((IntPtr)(&table_small_pages[hash_function_result])) & (~((64 - 1))); 
                    cache->processMemOpFromCore(
                        lock_signal,
                        Core::mem_op_t::READ,
                        cache_address, 0,
                        data_buf, data_length,
                        modeled,
                        count, CacheBlockInfo::block_type_t::PAGE_TABLE, SubsecondTime::Zero());
                t_end = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
                getShmemPerfModel()->setElapsedTime(ShmemPerfModel::_USER_THREAD,t_start);
                if(nuca){
                    nuca->markTranslationMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
                    mem_manager->getCache(MemComponent::component_t::L1_DCACHE)->markMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
                    mem_manager->getCache(MemComponent::component_t::L2_CACHE)->markMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
                }
                t_start_1 = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD); 
                cache_address = ((IntPtr)(&table_big_pages[hash_function_result])) & (~((64 - 1))); 
                    cache->processMemOpFromCore(
                        lock_signal,
                        Core::mem_op_t::READ,
                        cache_address, 0,
                        data_buf, data_length,
                        modeled,
                        count, CacheBlockInfo::block_type_t::PAGE_TABLE, SubsecondTime::Zero());
                t_end_1 = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
                delay += std::max(t_end-t_start,t_end_1-t_start_1) ;
                getShmemPerfModel()->setElapsedTime(ShmemPerfModel::_USER_THREAD,t_start);
                if(nuca){
                    nuca->markTranslationMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
                    mem_manager->getCache(MemComponent::component_t::L1_DCACHE)->markMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
                    mem_manager->getCache(MemComponent::component_t::L2_CACHE)->markMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
                }
                getShmemPerfModel()->incrElapsedTime(std::max(t_end-t_start,t_end_1-t_start_1),ShmemPerfModel::_USER_THREAD);
                //Code I am bored to make a function
                if(hash_function_result==initial_hash_function_result){
                    //Simulate disk access
                    //delay += static SubsecondTime
                    delay+=latency_for_disk_access.getLatency();
                    
                    return delay;
                }
                hash_function_result++;
                
                if(hash_function_result>=(int)pow(2,table_size_in_bits)){
                    hash_function_result=0;
                }
                //Code I am bored to make a function
            
            }
        }

        //We are in page-fault scenario
        int random_number=rand()%100;
        if(random_number<small_page_percentage){
            table_small_pages[small_pages_page_fault_address].empty=false;
            table_small_pages[small_pages_page_fault_address].vpn=address>>(small_page_size_in_bits+2);
            table_small_pages[small_pages_page_fault_address].ptes[(address>>small_page_size_in_bits)%4]=(int*)malloc((int)pow(2,small_page_size_in_bits)*sizeof(int));
            t_start = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);                
            cache_address = ((IntPtr)(&table_small_pages[small_pages_page_fault_address].ptes[(address>>small_page_size_in_bits)%4][address%(int)pow(2,small_page_size_in_bits)])) & (~((64 - 1))); 
                cache->processMemOpFromCore(
                    lock_signal,
                    Core::mem_op_t::READ,
                    cache_address, 0,
                    data_buf, data_length,
                    modeled,
                    count, CacheBlockInfo::block_type_t::PAGE_TABLE, SubsecondTime::Zero());
            t_end = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
            delay += t_end - t_start ;
            if(nuca){
                nuca->markTranslationMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
                mem_manager->getCache(MemComponent::component_t::L1_DCACHE)->markMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
                mem_manager->getCache(MemComponent::component_t::L2_CACHE)->markMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
            }
            //std::cout<<"Page alloc in small pages table! "<<address<<"\n";
        }
        else{
            stats.number_of_2MB_pages++;
            table_big_pages[big_pages_page_fault_address].empty=false;
            table_big_pages[big_pages_page_fault_address].vpn=address>>(big_page_size_in_bits+2);
            table_big_pages[big_pages_page_fault_address].ptes[(address>>big_page_size_in_bits)%4]=(int*)malloc((int)pow(2,big_page_size_in_bits)*sizeof(int));
            t_start = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);                
            cache_address = ((IntPtr)(&table_big_pages[big_pages_page_fault_address].ptes[(address>>big_page_size_in_bits)%4][address%(int)pow(2,big_page_size_in_bits)])) & (~((64 - 1))); 
                cache->processMemOpFromCore(
                    lock_signal,
                    Core::mem_op_t::READ,
                    cache_address, 0,
                    data_buf, data_length,
                    modeled,
                    count, CacheBlockInfo::block_type_t::PAGE_TABLE, SubsecondTime::Zero());
            t_end = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
            delay += t_end - t_start ;
            if(nuca){
                nuca->markTranslationMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
                mem_manager->getCache(MemComponent::component_t::L1_DCACHE)->markMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
                mem_manager->getCache(MemComponent::component_t::L2_CACHE)->markMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
            }
            //std::cout<<"Page alloc in big pages table!"<<address<<"\n";
        }
        
        //Code I am bored to make a function
        getShmemPerfModel()->setElapsedTime(ShmemPerfModel::_USER_THREAD,start);

        UInt64 vpn = address >> init_walk_functional(address);
        track_per_page_ptw_latency(vpn,delay);
        //std::cout<<"Page Fault\n";
        return delay;
        
        
    }
    bool PageTableHashDontCache::isPageFault(IntPtr address){
        //std::cout<<"Is page fault "<<address<<"\n";
        int hash_function_result=hash_function(address);
        int initial_hash_function_result=hash_function_result;
        SubsecondTime access_delay=SubsecondTime::Zero();
        while(!table_small_pages[hash_function_result].empty || !table_big_pages[hash_function_result].empty){
            if(table_small_pages[hash_function_result].vpn == address>>(small_page_size_in_bits+2)){
                //std::cout<<"Returns not page fault\n";
                return false;
            }
            else if(table_big_pages[hash_function_result].vpn == address>>(big_page_size_in_bits+2)){
                //std::cout<<"Returns not page fault\n";
                return false;
            }
            else{
                if(hash_function_result==initial_hash_function_result){
                    //std::cout<<"Returns page fault\n";
                    return true;
                }
                hash_function_result++;
                
                if(hash_function_result >= (int)pow(2,table_size_in_bits)){
                    hash_function_result=0;
                }
            }
        }
        //std::cout<<"Returns page fault\n";
        return true;

    }
    int PageTableHashDontCache::init_walk_functional(IntPtr address){
        //std::cout<<"Is init walk functional "<<address<<"\n";
        int hash_function_result=hash_function(address);
        int initial_hash_function_result=hash_function_result;
        SubsecondTime access_delay=SubsecondTime::Zero();
        while(!table_small_pages[hash_function_result].empty || !table_big_pages[hash_function_result].empty){
            if(table_small_pages[hash_function_result].vpn == address>>(small_page_size_in_bits+2)){
                return small_page_size_in_bits;
            }
            else if(table_big_pages[hash_function_result].vpn == address>>(big_page_size_in_bits+2)){
                return big_page_size_in_bits;
            }
            else{
                if(hash_function_result==initial_hash_function_result){
                    
                    return small_page_size_in_bits;
                }
                hash_function_result++;
                
                if(hash_function_result >= (int)pow(2,table_size_in_bits)){
                    hash_function_result=0;
                }
            }
        }
        return small_page_size_in_bits;
    }
}