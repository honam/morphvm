/* Copyright (c) IAIK, Graz University of Technology, 2015.
 * All rights reserved.
 * Contact: http://opensource.iaik.tugraz.at
 * 
 * This file is part of the Merkle Tree Library.
 * 
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and SIC. For further information
 * contact us at http://opensource.iaik.tugraz.at.
 * 
 * Alternatively, this file may be used under the terms of the GNU General
 * Public License as published by the Free Software Foundation version 2.
 * 
 * The Merkle Tree Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with the Merkle Tree Library. If not, see <http://www.gnu.org/licenses/>.
 */
/*!
 * \file
 * \brief Implements the Merkle Tree data type.
 */
#include "merkletree.h"
#include "mt_crypto.h"


#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifdef MT_DEBUG
#define DEBUG(m, ...) do {printf(m, __VA_ARGS__);} while(0);
#else
#define DEBUG(m, ...)
#endif

 uint32_t round_next_power_two(uint32_t v)
{
  v--;
  v |= v >> 1;
  v |= v >> 2;
  v |= v >> 4;
  v |= v >> 8;
  v |= v >> 16;
  v++;
  v += (v == 0); // handle v == 0 edge case
  return v;
}

//----------------------------------------------------------------------

 int is_power_of_two(uint32_t v)
{
  return (v != 0) && ((v & (v - 1)) == 0);
}

//----------------------------------------------------------------------
mt_al_t *mt_al_create(void)
{
  return (mt_al_t *)calloc(1, sizeof(mt_al_t));
}

//----------------------------------------------------------------------
void mt_al_delete(mt_al_t *mt_al)
{
  free(mt_al->store);
  free(mt_al);
}

//----------------------------------------------------------------------
mt_error_t mt_al_add(mt_al_t *mt_al, const mt_hash_t hash)
{
  // this can only happen due to outside interference.
  assert(mt_al->elems < MT_AL_MAX_ELEMS); // number of elements is smaller than the maximum possible elements => avoid overflowing
  // check if mt or hash is defined 
  if (!(mt_al && hash)) {
    return MT_ERR_ILLEGAL_PARAM;
  }
  
  if (mt_al->elems == 0) {
    // Add first element
    mt_al->store = (uint8_t *) malloc(HASH_LENGTH);
    if (!mt_al->store) {
      return MT_ERR_OUT_Of_MEMORY;
    }
  } else if (is_power_of_two(mt_al->elems)) {
    // Need more memory
    // Prevent integer overflow during size calculation
    if (((mt_al->elems << 1) < mt_al->elems) // not sure how this is possible?
        || (mt_al->elems << 1 > MT_AL_MAX_ELEMS)) { // check if overflow?
      return MT_ERR_ILLEGAL_STATE;
    }
    size_t alloc = mt_al->elems * 2 * HASH_LENGTH;
    uint8_t *tmp = (uint8_t*)realloc(mt_al->store, alloc); //resize the leave lists
    if (!tmp) {
      return MT_ERR_OUT_Of_MEMORY;
    }
// #ifdef DBUG
//    fprintf(stderr, "Allocated memory: %x, Old: %p, New: %p\n", alloc / HASH_LENGTH,
//        mt_al->store, tmp);
// #endif 
    mt_al->store = tmp;
  }
  memcpy(&mt_al->store[mt_al->elems * HASH_LENGTH], hash, HASH_LENGTH);
  mt_al->elems += 1;
  return MT_SUCCESS;
}

//----------------------------------------------------------------------
mt_error_t mt_al_update(const mt_al_t *mt_al, const mt_hash_t hash,
    const uint32_t offset)
{
  // this can only happen due to outside interference.
  assert(mt_al->elems < MT_AL_MAX_ELEMS);
  if (!(mt_al && hash && offset < mt_al->elems)) {
    return MT_ERR_ILLEGAL_PARAM;
  }
  memcpy(&mt_al->store[offset * HASH_LENGTH], hash, HASH_LENGTH);
  return MT_SUCCESS;
}

//----------------------------------------------------------------------
mt_error_t mt_al_update_if_exists(const mt_al_t *mt_al, const mt_hash_t hash,
    const uint32_t offset)
{
  assert(mt_al->elems < MT_AL_MAX_ELEMS);
  if (!(mt_al && hash)) {
    return MT_ERR_ILLEGAL_PARAM;
  }
  // nothing is updated if offset is larger than existing elements in the leaves
  if (offset >= mt_al->elems) {
    return MT_SUCCESS;
  }
  memcpy(&mt_al->store[offset * HASH_LENGTH], hash, HASH_LENGTH);
  return MT_SUCCESS;
}

//----------------------------------------------------------------------
mt_error_t mt_al_add_or_update(mt_al_t *mt_al, const mt_hash_t hash,
    const uint32_t offset)
{
  // this can only happen due to outside interference.
  assert(mt_al->elems < MT_AL_MAX_ELEMS);
  if (!(mt_al && hash) || offset > mt_al->elems) {
    return MT_ERR_ILLEGAL_PARAM;
  }
  if (offset == mt_al->elems) {
    return mt_al_add(mt_al, hash);
  } else {
    return mt_al_update(mt_al, hash, offset);
  }
}

//----------------------------------------------------------------------
mt_error_t mt_al_truncate(mt_al_t *mt_al, const uint32_t elems)
{
  // this can only happen due to outside interference.
  assert(mt_al->elems < MT_AL_MAX_ELEMS);
  if (!(mt_al && elems < mt_al->elems)) {
    return MT_ERR_ILLEGAL_PARAM;
  }
  mt_al->elems = elems;
  if (elems == 0) {
    free(mt_al->store);
    return MT_SUCCESS;
  }
  uint32_t alloc = round_next_power_two(elems) * HASH_LENGTH;
  uint8_t *tmp = (uint8_t *)realloc(mt_al->store, alloc);
  if (!tmp) {
    return MT_ERR_OUT_Of_MEMORY;
  }
//  fprintf(stderr, "Allocated memory: %x, Old: %p, New: %p\n",
//      alloc / HASH_LENGTH, mt_al->store, tmp);
  mt_al->store = tmp;
  return MT_SUCCESS;
}

//----------------------------------------------------------------------
 uint8_t *mt_al_get(const mt_al_t *mt_al, const uint32_t offset)
{
  // this can only happen due to outside interference.
  assert(mt_al->elems < MT_AL_MAX_ELEMS);
  if (!(mt_al && offset < mt_al->elems)) {
    return NULL;
  }
  return &mt_al->store[offset * HASH_LENGTH];
}

//----------------------------------------------------------------------
void mt_al_print_hex_buffer(const uint8_t *buffer, const size_t size)
{
  if (!buffer) {
    fprintf(stderr,
        "[ERROR][mt_al_print_hex_buffer]: Merkle Tree array list is NULL");
    return;
  }
  for (size_t i = 0; i < size; ++i) {
    printf("%02X", buffer[i]);
  }
}

//----------------------------------------------------------------------
char *mt_al_sprint_hex_buffer(const uint8_t *buffer, const size_t size)
{
  if (!buffer) {
    fprintf(stderr,
        "[ERROR][mt_al_sprint_hex_buffer]: Merkle Tree array list is NULL");
    return NULL;
  }
  size_t to_alloc = size * (sizeof(char) * 2) + 1;
  char *str = (char*)malloc(to_alloc);
  for (size_t i = 0; i < size; ++i) {
    snprintf((str + (i*2)), 3, "%02X", buffer[i]);
  }
  return str;
}

//----------------------------------------------------------------------
void mt_al_print(const mt_al_t *mt_al)
{
  // this can only happen due to outside interference.
  assert(mt_al->elems < MT_AL_MAX_ELEMS);
  if (!mt_al) {
    fprintf(stderr, "[ERROR][mt_al_print]: Merkle Tree array list is NULL");
    return;
  }
  printf("Then number of elements in the list is: ");
  printf("%08X\n[", (unsigned int)mt_al->elems);
  for (uint32_t i = 0; i < mt_al->elems; ++i) {
    mt_al_print_hex_buffer(&mt_al->store[i * HASH_LENGTH], HASH_LENGTH);
    printf("\n");
  }
  printf("]\n");
}


//----------------------------------------------------------------------
mt_t *mt_create(void)
{
  mt_t *mt = (mt_t*)calloc(1, sizeof(mt_t));
  if (!mt) {
    return NULL;
  }
  for (uint32_t i = 0; i < TREE_LEVELS; ++i) {
    mt_al_t *tmp = mt_al_create();
    if (!tmp) {
      for (uint32_t j = 0; j < i; ++j) {
        mt_al_delete(mt->level[j]);
      }
      free(mt);
      return NULL;
    }
    mt->level[i] = tmp;
  }
  return mt;
}

//----------------------------------------------------------------------
void mt_delete(mt_t *mt)
{
  if (!mt) {return;}
  for (uint32_t i = 0; i < TREE_LEVELS; ++i) {
    mt_al_delete(mt->level[i]);
  }
  free(mt);
}

/*!
 * \brief Determines if the given index points to a right node in the tree
 * @param offset the index of the node
 * @return true if the given index is a right node; false otherwise
 */
 int mt_right(uint32_t offset)
{
  // odd index means we are in the right subtree
  return offset & 0x01;
}

/*!
 * \brief Determines if the given index points to a left node in the tree
 * @param offset the index of the node
 * @return true if the given index is a left node; false otherwise
 */
 int mt_left(uint32_t offset)
{
  // even index means we are in the left subtree
  return !(offset & 0x01);
}

/*!
 * \brief Copies len bytes from tag into hash
 *
 * If len is less than HASH_LENGTH, then the rest of the hash will be filled
 * with zeros. This function ensures that len is less than HASH_LENGTH.
 *
 * @param hash[out] the hash to initialize
 * @param tag[in] the tag to copy from
 * @param len[in] the number of bytes to copy from tag into hash
 */
 void mt_init_hash(mt_hash_t hash, const uint8_t *tag, const size_t len)
{
  assert(hash && tag && len <= HASH_LENGTH);
  memset(hash, 0, HASH_LENGTH);
  memcpy(hash, tag, len);
}

//----------------------------------------------------------------------
mt_error_t mt_add(mt_t *mt, const uint8_t *tag, const size_t len)
{
  if (!(mt && tag && len <= HASH_LENGTH)) {
    return MT_ERR_ILLEGAL_PARAM;
  }
  uint8_t message_digest[HASH_LENGTH];
  mt_init_hash(message_digest, tag, len);
  DEBUG("[MT_ADD][a][@%d] %s\n", mt->elems,
      mt_al_sprint_hex_buffer(message_digest, HASH_LENGTH));
  MT_ERR_CHK(mt_al_add(mt->level[0], message_digest));
  mt->elems += 1;
  if (mt->elems == 1) {
    return MT_SUCCESS;
  }
  uint32_t q = mt->elems - 1;
  uint32_t l = 0;         // level
  while (q > 0 && l < TREE_LEVELS) {
    if (mt_right(q)) {
      uint8_t const * const left = mt_al_get(mt->level[l], q - 1);
      MT_ERR_CHK(mt_hash(left, message_digest, message_digest));
      MT_ERR_CHK(
          mt_al_add_or_update(mt->level[l + 1], message_digest, (q >> 1)));
    }
    q >>= 1;
    l += 1;
  }
  assert(!memcmp(message_digest, mt_al_get(mt->level[l], q), HASH_LENGTH));
  DEBUG("[MT_ADD][r][@%d] %s\n", l,
      mt_al_sprint_hex_buffer(message_digest, HASH_LENGTH))
  return MT_SUCCESS;
}


//----------------------------------------------------------------------
uint32_t mt_get_size(const mt_t *mt)
{
  if (!mt) {
    return MT_ERR_ILLEGAL_PARAM;
  }
  return mt_al_get_size(mt->level[0]);
}

//----------------------------------------------------------------------
int mt_exists(mt_t *mt, const uint32_t offset)
{
  if (!mt || offset > MT_AL_MAX_ELEMS) {
    return MT_ERR_ILLEGAL_PARAM;
  }
  return (mt_al_get(mt->level[0], offset) != NULL);
}

//----------------------------------------------------------------------
 uint32_t hasNextLevelExceptRoot(mt_t const * const mt, uint32_t cur_lvl)
{
  if (!mt) {
    return 0;
  }
  return (cur_lvl + 1 < TREE_LEVELS - 1)
      & (mt_al_get_size(mt->level[(cur_lvl + 1)]) > 0);
}

//----------------------------------------------------------------------
 uint8_t *findRightNeighbor(const mt_t *mt, uint32_t offset,
    int32_t l)
{
  if (!mt) {
    return NULL;
  }
  do {
    if (offset < mt_al_get_size(mt->level[l])) {
      //*addr = mt_al_get(mt->level[l], offset); // getting the address
      return mt_al_get(mt->level[l], offset);
    }
    l -= 1;
    offset <<= 1;
  } while (l > -1);
  // This can happen, if there is no neighbor.
  return NULL;
}

//----------------------------------------------------------------------
mt_error_t mt_verify(const mt_t *mt, const uint8_t *tag, const size_t len,
    const uint32_t offset, uint64_t *addrs, uint8_t *level)
{
  // printf("");
  if (!(mt && tag && len <= HASH_LENGTH && (offset < mt->elems))) {
    return MT_ERR_ILLEGAL_PARAM;
  }
  uint8_t message_digest[HASH_LENGTH];
  mt_init_hash(message_digest, tag, len);
  uint32_t q = offset;
  uint32_t l = 0;         // level
  //uint64_t addrs[TREE_LEVELS]; // 
  while (hasNextLevelExceptRoot(mt, l)) {
    if (!(q & 0x01)) { // left subtree
      // If I am the left neighbor (even index), we need to check if a right
      // neighbor exists.
      const uint8_t *right;
      if ((right = findRightNeighbor(mt, q + 1, l)) != NULL) {
        addrs[l] = (uint64_t) right;
        MT_ERR_CHK(mt_hash(message_digest, right, message_digest));
      }
    } else {           // right subtree
      // In the right subtree, there must always be a left neighbor!
      uint8_t const * const left = mt_al_get(mt->level[l], q - 1);
      addrs[l] = (uint64_t)left; 
      MT_ERR_CHK(mt_hash(left, message_digest, message_digest));
    }
    q >>= 1;
    l += 1;
  }
  //mt_print_hash(message_digest);
  addrs[l] = (uint64_t) mt_al_get(mt->level[l], q); // root is also kept
  // l++;
  *level = l+1; // giving the number of addresses for verifcation

  int r = memcmp(message_digest, mt_al_get(mt->level[l], q), HASH_LENGTH);
  if (r) {
    return MT_ERR_ROOT_MISMATCH;
  } else {
    return MT_SUCCESS;
  }
}

//----------------------------------------------------------------------
mt_error_t mt_update(const mt_t *mt, const uint8_t *tag, const size_t len,
    const uint32_t offset)
{
  if (!(mt && tag && len <= HASH_LENGTH && (offset < mt->elems))) {
    return MT_ERR_ILLEGAL_PARAM;
  }
  uint8_t message_digest[HASH_LENGTH];
  mt_init_hash(message_digest, tag, len);
  DEBUG("[MT_UPT][u][@%d] %s\n", offset,
      mt_al_sprint_hex_buffer(message_digest, HASH_LENGTH))
  MT_ERR_CHK(mt_al_update(mt->level[0], message_digest, offset));
  uint32_t q = offset;
  uint32_t l = 0;         // level
  while (hasNextLevelExceptRoot(mt, l)) {
    if (mt_left(q)) { // left subtree
      // If I am the left neighbor (even index), we need to check if a right
      // neighbor exists.
      const uint8_t *right;
      if ((right = findRightNeighbor(mt, q + 1, l)) != NULL) {
        MT_ERR_CHK(mt_hash(message_digest, right, message_digest));
      }
    } else {           // right subtree
      // In the right subtree, there must always be a left neighbor!
      uint8_t const * const left = mt_al_get(mt->level[l], q - 1);
      MT_ERR_CHK(mt_hash(left, message_digest, message_digest));
    }
    q >>= 1;
    l += 1;
    MT_ERR_CHK(mt_al_update_if_exists(mt->level[l], message_digest, q));
  }
  assert(!memcmp(message_digest, mt_al_get(mt->level[l], q), HASH_LENGTH));
  DEBUG("[MT_UPT][r][@%d] %s\n", l,
      mt_al_sprint_hex_buffer(message_digest, HASH_LENGTH))
  return MT_SUCCESS;
}

//----------------------------------------------------------------------
mt_error_t mt_get_root(mt_t *mt, mt_hash_t root)
{
  if (!(mt && root)) {
    return MT_ERR_ILLEGAL_PARAM;
  }
  uint32_t l = 0;         // level
  while (hasNextLevelExceptRoot(mt, l)) {
    l += 1;
  }
  memcpy(root, mt_al_get(mt->level[l], 0), sizeof(mt_hash_t));
  return MT_SUCCESS;
}

//----------------------------------------------------------------------
void mt_print_hash(const mt_hash_t hash)
{
  if (!hash) {
    printf("[ERROR][mt_print_hash]: Hash NULL");
  }
  mt_al_print_hex_buffer(hash, HASH_LENGTH);
  printf("\n");
}

//----------------------------------------------------------------------
void mt_print(const mt_t *mt)
{
  if (!mt) {
    printf("[ERROR][mt_print]: Merkle Tree NULL");
    return;
  }
  for (uint32_t i = 0; i < TREE_LEVELS; ++i) {
    if (mt->level[i]->elems == 0) {
      return;
    }
    printf(
        "==================== Merkle Tree level[%02u]: ====================\n",
        (unsigned int)i);
    mt_al_print(mt->level[i]);
  }
}
