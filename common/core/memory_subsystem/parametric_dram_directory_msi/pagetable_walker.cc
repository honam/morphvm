#include "pagetable_walker.h"
#include <ctime>
#include "config.hpp"
#include "pwc.h"
#include "subsecond_time.h"
#include "nuca_cache.h"


namespace ParametricDramDirectoryMSI
{

	std::default_random_engine generator;
	std::uniform_int_distribution<long long int> distribution(1,0xA00000000000000);

	//https://spc.unige.ch/en/teaching/courses/algorithmes-probabilistes/random-numbers-week-1/
	

	PageTableWalker::PageTableWalker(int _core_id, int _page_size, ShmemPerfModel* _m_shmem_perf_model, PWC* _pwc, bool _page_walk_cache_enabled)
	{
		CR3 = 0x1000000;
		CR3 = CR3*(core_id+1);
		data_size = 8;		
		page_size = _page_size;
		String name = "PTW";
		m_shmem_perf_model = _m_shmem_perf_model;
		page_walk_cache_enabled = _page_walk_cache_enabled;
		core_id = _core_id;
		stats.number_of_2MB_pages=0;
		if( page_walk_cache_enabled){

			pwc = _pwc; // Instantiate page walk caches

		}

		srand(time(0));

		ptw_latency_heatmap = (int*) calloc(5, sizeof(int)); // Used to trigger Per-page PTW stats 

		bzero(&stats, sizeof(stats));
   		
		registerStatsMetric(name, core_id, "page_walks", &stats.page_walks);
   		registerStatsMetric(name, core_id, "total-PMLE4-latency", &stats.total_PMLE4);
   		registerStatsMetric(name, core_id, "total-PDPE-latency", &stats.total_PDPE);
		registerStatsMetric(name, core_id, "total-PD-latency", &stats.total_PD);
   		registerStatsMetric(name, core_id, "total-PT-latency", &stats.total_PT);
		registerStatsMetric(name, core_id, "number-of-2MB-pages", &stats.number_of_2MB_pages);
		registerStatsMetric(name, core_id, "ptw_latency_heatmap", &ptw_latency_heatmap, true);

	}

	SubsecondTime PageTableWalker::init_walk(IntPtr address, UtopiaCache* shadow_cache,CacheCntlr *_cache, Core::lock_signal_t _lock_signal, Byte* _data_buf, UInt32 _data_length, bool _modeled, bool _count)
	{
		
		// Setup the step walk functions
		stats.page_walks++;
		bzero(&current_lat, sizeof(current_lat));

		cache = _cache;
		modeled = _modeled;
		count = _count;
		data_buf = _data_buf;
		vpn = address >> page_size;
		data_length = _data_length;
		lock_signal = _lock_signal;
		latency = SubsecondTime::Zero();
		
		//printf("Page Walk latency %ld \n",SubsecondTime::divideRounded(current_lat,period));

		step_walk(address,CR3,0,0,0,4);

		return latency;
	};

	void PageTableWalker::perform_pagetable_access(IntPtr address, int level, bool pwc_available){
		IntPtr pwc_address; 
		IntPtr cache_address;

		pwc_address = address & (~((8 - 1))); //8 Byte alignment
		cache_address = address & (~((64 - 1))); //8 Byte alignment


		LOG_PRINT("Accessing cache for page walk: 0x%x \n",address );		
		SubsecondTime total_latency;
		SubsecondTime t_start = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);

				
		if(pwc_available){
			PWC::where_t pwc_where;
			bool pwc_hit = false;
			if(page_walk_cache_enabled && level != 1 )
			{
				pwc_where = pwc->lookup(address, t_start ,true, level, count);
				if( pwc_where == PWC::HIT ) pwc_hit = true; 

			}
			if(pwc_hit == true){

				total_latency = pwc->access_latency.getLatency(); 

			}
			else{
				t_start = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);

				cache->processMemOpFromCore(
					lock_signal,
					Core::mem_op_t::READ,
					cache_address, 0,
					data_buf, data_length,
					modeled,
					count,CacheBlockInfo::block_type_t::PAGE_TABLE, SubsecondTime::Zero()
					);

				SubsecondTime t_end = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
				total_latency = t_end - t_start + pwc->miss_latency.getLatency();

				if(nuca){

					nuca->markTranslationMetadata(address,CacheBlockInfo::block_type_t::PAGE_TABLE);

				}
				
			}
		}
		

		switch(level){

			case 4:
				stats.total_PMLE4 += total_latency;
				current_lat += total_latency;
				latency += total_latency;
				break;
			case 3: 
				stats.total_PDPE += total_latency;
				current_lat += total_latency;
				latency += total_latency;
				break;
			case 2: 
				stats.total_PD += total_latency;
				current_lat += total_latency;
				latency += total_latency;
				break;
			case 1:
				stats.total_PT += total_latency;
				current_lat += total_latency;
				latency += total_latency;
				if(count){
					SubsecondTime period = SubsecondTime::SEC() / (2.6*1000000000);
					UInt64 cycles = SubsecondTime::divideRounded(current_lat, period);
					//printf("Page Walk latency %ld \n",SubsecondTime::divideRounded(current_lat,period));
					if(per_page_ptw_latency.find(vpn) != per_page_ptw_latency.end())
						per_page_ptw_latency[vpn] += cycles ;
					else
						per_page_ptw_latency[vpn ] = cycles ;
				}
				//std::cout<<"CYCLES "<<cycles<<std::endl;
				current_lat-=current_lat;
				break;

		}
			
		return ;
	};

	IntPtr PageTableWalker::query_pagetable(int offset, IntPtr pointer_PMLE4, IntPtr pointer_PDPE, IntPtr pointer_PD, IntPtr pointer_PT, int level){

		if ( level == 3 ){
			std::pair <int,IntPtr> pair_table(offset,pointer_PMLE4);   // value init
			if(PDPE_table.find(pair_table) == PDPE_table.end())
				PDPE_table[pair_table] = distribution(generator) % 0xA00000000000000 + 0x1000000;

			//printf("Address for PDPE 0x%lx \n", PDPE_table[pair_table]);
			return PDPE_table[pair_table];
		}
		else if ( level == 2 ){
			std::tuple <int,IntPtr,IntPtr> pair_table(offset,pointer_PMLE4,pointer_PDPE);   // value init
			if(PD_table.find(pair_table) == PD_table.end())
				PD_table[pair_table] = distribution(generator) % 0xA00000000000000  + 0x1000000;

			//printf("Address for PD 0x%lx \n", PD_table[pair_table]);
			return PD_table[pair_table];
		}
		else if ( level == 1 ){
			std::tuple <int,IntPtr,IntPtr,IntPtr> pair_table(offset,pointer_PMLE4,pointer_PDPE,pointer_PD);   // value init
			if(PT_table.find(pair_table) == PT_table.end())
				PT_table[pair_table] = distribution(generator) % 0xA00000000000000  + 0x1000000;

			//printf("Address for PT 0x%lx \n", PT_table[pair_table]);
			return PT_table[pair_table];
		}
	}


	void PageTableWalker::step_walk(IntPtr address, IntPtr pointer_PMLE4, IntPtr pointer_PDPE, IntPtr pointer_PD, IntPtr pointer_PT, int level){

			
		if ( level == 4 ){  // Page-Map-Level-4 HAS 512 entries with 8 Bytes -> Take 9 MSB

			IntPtr offset_PML4;
			
			offset_PML4 = pointer_PMLE4 + ((address & (0xFF8000000000))>>39)*data_size;
			LOG_PRINT("Accessing PML4: 0x%x \n", offset_PML4);
			perform_pagetable_access(offset_PML4,4,true);

			step_walk(address, offset_PML4, 0,0,0,3);
		}
		else if ( level == 3 ){ // Page-Directory-Pointer Offset 
		
			IntPtr offset_PDPE;
			offset_PDPE = ((address & (0x007FC0000000))>>30)*data_size;
			LOG_PRINT("Accessing PDPE: 0x%x \n",offset_PDPE );
			IntPtr address_PDPE  = query_pagetable(offset_PDPE, pointer_PMLE4, 0, 0, 0, 3 );		
			perform_pagetable_access(address_PDPE,3,true);	
			step_walk(address, pointer_PMLE4, address_PDPE, 0,0, 2);

		}
		else if ( level == 2){ // Page-Directory Offset
		
			IntPtr offset_PD;
			offset_PD = ((address & (0x00003FE00000))>>21)*data_size;
			LOG_PRINT("Accessing PD: 0x%x \n", offset_PD);
			IntPtr address_PD  = query_pagetable(offset_PD, pointer_PMLE4, pointer_PDPE, 0, 0, 2 );		
			perform_pagetable_access(address_PD,2,true);	
			step_walk(address, pointer_PMLE4, pointer_PDPE, address_PD, 0, 1);
		}
		else if ( level == 1){ // Page-Table Offset

			IntPtr offset_PT;
			offset_PT = ((address & (0x0000001FF000))>>12)*data_size;
			LOG_PRINT("Accessing PT: 0x%x \n", offset_PT,true);
			IntPtr address_PT  = query_pagetable(offset_PT, pointer_PMLE4, pointer_PDPE, pointer_PD, 0, 1 );		
			perform_pagetable_access(address_PT,1,true);	
			return ;
		}
	};


	void PageTableWalker::track_per_page_ptw_latency(int vpn, SubsecondTime current_lat){
		//std::cout<<"Works!\n";
		SubsecondTime period = SubsecondTime::SEC() / (2.6*1000000000);
		UInt64 cycles = SubsecondTime::divideRounded(current_lat, period);
		if(per_page_ptw_latency.find(vpn) != per_page_ptw_latency.end())
			per_page_ptw_latency[vpn] += cycles ;
		else
			per_page_ptw_latency[vpn] = cycles ;

	}


}
