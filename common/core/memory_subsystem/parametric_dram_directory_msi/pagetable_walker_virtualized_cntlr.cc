#include "pagetable_walker_virtual.h"
#include <ctime>
#include "config.hpp"
#include "pwc.h"
#include "subsecond_time.h"
#include "simulator.h"
#include "core_manager.h"
#include "memory_manager.h"
#include "utopia_cache_template.h"


namespace ParametricDramDirectoryMSI
{
    PageTableWalkerVirtual::PageTableWalkerVirtual(int core_id, int _page_size, ShmemPerfModel* _m_shmem_perf_model, PWC* _pwc, bool _page_walk_cache_enabled):
        PageTableWalker(core_id, _page_size, _m_shmem_perf_model, _pwc, _page_walk_cache_enabled)
	{
		String name = "PTWvirtual";
		utopia_enabled = Sim()->getCfg()->getBool("perf_model/utopia/enabled");

        gCR3 = 0x1234567*(core_id+1);
		latency_virtual = SubsecondTime::Zero();
		virtual_page_walks= 0;
		utopia_hits = 0;
		utopia_misses = 0;

		registerStatsMetric(name, core_id, "virtual_page_walks", &virtual_page_walks);
		registerStatsMetric(name, core_id, "utopia_hits", &utopia_hits);
		registerStatsMetric(name, core_id, "utopia_misses", &utopia_misses);
		registerStatsMetric(name, core_id, "latency", &latency_virtual);
	}

    SubsecondTime PageTableWalkerVirtual::init_walk(IntPtr address,UtopiaCache* shadow_cache, CacheCntlr *_cache, Core::lock_signal_t _lock_signal, Byte* _data_buf, UInt32 _data_length, bool _modeled, bool _count)
	{
		// Setup the step walk functions
		virtual_page_walks++;
		bzero(&current_lat, sizeof(current_lat));

		cache = _cache;
		modeled = _modeled;
		count = _count;
		data_buf = _data_buf;
		data_length = _data_length;
		lock_signal = _lock_signal;
		latency = SubsecondTime::Zero();
		
		//printf("Initializing Virtual Page Walk\n");

		step_walk_virtual(address, gCR3, 0, 0, 0, 4, true, false);

		latency_virtual += latency;

		return latency;
	};

    void PageTableWalkerVirtual::step_walk_virtual(IntPtr address, IntPtr pointer_PMLE4, IntPtr pointer_PDPE, IntPtr pointer_PD, IntPtr pointer_PT, int level, bool virtual_walk, bool nested){

	}


}