#ifndef PTWV2_H
#define PTWV2_H

#include "page_table_walker_types.h"
#include "pagetable_walker.h"
#include "cache_cntlr.h"
#include "subsecond_time.h"
#include "fixed_types.h"
#include "memory_manager.h"
#include <stdint.h>
#include "utopia_cache_template.h"
#include "merkletree.h"

using namespace PtwTypes;

namespace ParametricDramDirectoryMSI{

    class PageTableWalkerRadix: public PageTableWalker{

        public:
            
            struct{
                int number_of_levels;
                int *address_bit_indices;
                int *hit_percentages;
                bool integrity_enabled;
                // bool enc_enabled;
                // int integrity_level;
                // int verify_level;
                verify_struct* verify_metadata;
            } stats_radix;
            UtopiaCache* shadow_cache;
            Core* core;
            CacheCntlr *cache;
            PageTableWalkerRadix(int number_of_levels,Core* _core,ShmemPerfModel* _m_shmem_perf_model,int *level_bit_indices,int *level_percentages, PWC* pwc, bool pwc_enabled,UtopiaCache* shadow_cache, verify_struct* verify_metadata);
            ptw_table* starting_table;
            ShmemPerfModel* m_shmem_perf_model;
            SubsecondTime *latency_per_level;
            SubsecondTime init_walk(IntPtr address, UtopiaCache* shadow_cache, CacheCntlr *_cache,Core::lock_signal_t lock_signal,Byte* data_buf, UInt32 data_length,bool modeled, bool count) ;
            SubsecondTime InitializeWalkRecursive(IntPtr address,int level,ptw_table* new_table,Core::lock_signal_t lock_signal,Byte* data_buf, UInt32 data_length,bool modeled, bool count);
            int init_walk_functional(IntPtr address);
            int init_walk_recursive_functional(IntPtr address,int level,ptw_table* new_table);
            bool init_ptw_entry_verification(ptw_table_entry* talbe_entry);
            bool isPageFault(IntPtr address);
            bool isPageFaultHelper(uint64_t address,int level,ptw_table* new_table);
            bool isInitVerify(HitWhere::where_t hit_where, int level, verify_struct* verify_metadata);
    };

}
#endif 