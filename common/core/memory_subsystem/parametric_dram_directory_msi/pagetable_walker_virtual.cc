#include "pagetable_walker_virtual.h"
#include <ctime>
#include "config.hpp"
#include "pwc.h"
#include "subsecond_time.h"
#include "simulator.h"
#include "core_manager.h"
#include "memory_manager.h"


namespace ParametricDramDirectoryMSI
{
    PageTableWalkerVirtual::PageTableWalkerVirtual(int core_id, int _page_size, ShmemPerfModel* _m_shmem_perf_model, PWC* _pwc, bool _page_walk_cache_enabled):
        PageTableWalker(core_id, _page_size, _m_shmem_perf_model, _pwc, _page_walk_cache_enabled)
	{
		String name = "PTWvirtual";
		utopia_enabled = Sim()->getCfg()->getBool("perf_model/utopia/enabled");

        gCR3 = 0x1234567*(core_id+1);
		latency_virtual = SubsecondTime::Zero();
		virtual_page_walks= 0;
		utopia_hits = 0;
		utopia_misses = 0;

		registerStatsMetric(name, core_id, "virtual_page_walks", &virtual_page_walks);
		registerStatsMetric(name, core_id, "utopia_hits", &utopia_hits);
		registerStatsMetric(name, core_id, "utopia_misses", &utopia_misses);
		registerStatsMetric(name, core_id, "latency", &latency_virtual);
	}

    SubsecondTime PageTableWalkerVirtual::init_walk(IntPtr address, UtopiaCache* shadow_cache,CacheCntlr *_cache, Core::lock_signal_t _lock_signal, Byte* _data_buf, UInt32 _data_length, bool _modeled, bool _count)
	{
		// Setup the step walk functions
		virtual_page_walks++;
		bzero(&current_lat, sizeof(current_lat));

		cache = _cache;
		modeled = _modeled;
		count = _count;
		data_buf = _data_buf;
		vpn = address >> page_size;
		data_length = _data_length;
		lock_signal = _lock_signal;
		latency = SubsecondTime::Zero();
		
		//printf("Initializing Virtual Page Walk\n");

		step_walk_virtual(address, gCR3, 0, 0, 0, 4, true, false);

		latency_virtual += latency;

		return latency;
	};

    void PageTableWalkerVirtual::step_walk_virtual(IntPtr address, IntPtr pointer_PMLE4, IntPtr pointer_PDPE, IntPtr pointer_PD, IntPtr pointer_PT, int level, bool virtual_walk, bool nested){

		TranslationResult utopia_result;

		if ( level == 4 ){  // Page-Map-Level-4 HAS 512 entries with 8 Bytes -> Take 9 MSB

			IntPtr offset_PML4; 
			offset_PML4 = pointer_PMLE4 + ((address & (0xFF8000000000))>>39) * data_size; //GCR3 register
			LOG_PRINT("Accessing PML4: 0x%x \n", offset_PML4);
			if(virtual_walk && !nested)//if this is not nested translation and page table walker is for virtualization, perform a 4 level translation now in the host system
			{
				//printf("Starting 1 Nested PTW\n");
				if(utopia_enabled){
					
					MemoryManager* mmanager = (MemoryManager* ) (Sim()->getCoreManager()->getCoreFromID(core_id)->getMemoryManager());
					utopia_result = mmanager->accessUtopiaSubsystem(lock_signal,offset_PML4, data_buf, data_length, modeled, count);
					
					if(utopia_result.hitwhere == TranslationHitWhere::UTR_MISS){
						   Utopia *m_utopia;
						   UTR* m_utr;
						   m_utopia = Sim()->getUtopia();
   						   m_utr = m_utopia->getUtr(0);
						   SubsecondTime t_start = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
						   m_utr->allocate(offset_PML4,t_start,core_id);
						   utopia_misses++;
						   step_walk_virtual(offset_PML4, CR3, 0, 0, 0, 4, false, true); // CR3 is needed for the nested walk
					}
					else {
						utopia_hits++;
						latency += utopia_result.latency;
					}
				}
				else
					step_walk_virtual(offset_PML4, CR3, 0, 0, 0, 4, false, true); // CR3 is needed for the nested walk
			}
			perform_pagetable_access(offset_PML4,4, !virtual_walk);
			step_walk_virtual(address, offset_PML4, 0,0,0,3, virtual_walk, false);
		}
		else if ( level == 3 ){ // Page-Directory-Pointer Offset 
		
			IntPtr offset_PDPE;
			offset_PDPE = ((address & (0x007FC0000000))>>30)*data_size;
			LOG_PRINT("Accessing PDPE: 0x%x \n",offset_PDPE );
			IntPtr address_PDPE  = query_pagetable(offset_PDPE, pointer_PMLE4, 0, 0, 0, 3 );	
			if(virtual_walk && !nested)
			{
				if(utopia_enabled){

					MemoryManager* mmanager = (MemoryManager* ) (Sim()->getCoreManager()->getCoreFromID(core_id)->getMemoryManager());
					utopia_result = mmanager->accessUtopiaSubsystem(lock_signal,address_PDPE, data_buf, data_length, modeled, count);

					if(utopia_result.hitwhere == TranslationHitWhere::UTR_MISS){
						   Utopia *m_utopia;
						   UTR* m_utr;
						   m_utopia = Sim()->getUtopia();
   						   m_utr = m_utopia->getUtr(0);
						   SubsecondTime t_start = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
						   m_utr->allocate(address_PDPE,t_start,core_id);
						   utopia_misses++;
						   step_walk_virtual(address_PDPE, CR3, 0, 0, 0, 4, false, true);
					}
					else{
						   utopia_hits++;
						   latency += utopia_result.latency;

					}
				}
				else step_walk_virtual(address_PDPE, CR3, 0, 0, 0, 4, false, true);
			}	
			perform_pagetable_access(address_PDPE,3, !virtual_walk);	
			step_walk_virtual(address, pointer_PMLE4, address_PDPE, 0,0, 2, virtual_walk, false);

		}
		else if ( level == 2){ // Page-Directory Offset
		
			IntPtr offset_PD;
			offset_PD = ((address & (0x00003FE00000))>>21)*data_size;
			LOG_PRINT("Accessing PD: 0x%x \n", offset_PD);
			IntPtr address_PD  = query_pagetable(offset_PD, pointer_PMLE4, pointer_PDPE, 0, 0, 2 );	
			if(virtual_walk && !nested)
			{
				if(utopia_enabled){
					
					MemoryManager* mmanager = (MemoryManager* ) (Sim()->getCoreManager()->getCoreFromID(core_id)->getMemoryManager());
					utopia_result = mmanager->accessUtopiaSubsystem(lock_signal,address_PD, data_buf, data_length, modeled, count);
					
					if(utopia_result.hitwhere == TranslationHitWhere::UTR_MISS){
						   Utopia *m_utopia;
						   UTR* m_utr;
						   m_utopia = Sim()->getUtopia();
   						   m_utr = m_utopia->getUtr(0);
						   SubsecondTime t_start = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
						   m_utr->allocate(address_PD,t_start,core_id);
						   utopia_misses++;
						   step_walk_virtual(address_PD, CR3, 0, 0, 0, 4, false, true);
					}
					else{
						   
						   utopia_hits++;
						   latency += utopia_result.latency;

					}
				}
				else  step_walk_virtual(address_PD, CR3, 0, 0, 0, 4, false, true);
			}	
			perform_pagetable_access(address_PD,2, !virtual_walk);	
			step_walk_virtual(address, pointer_PMLE4, pointer_PDPE, address_PD, 0, 1, virtual_walk, false);
		}
		else if ( level == 1){ // Page-Table Offset

			IntPtr offset_PT;
			offset_PT = ((address & (0x0000001FF000))>>12)*data_size;
			LOG_PRINT("Accessing PT: 0x%x \n", offset_PT);
			IntPtr address_PT  = query_pagetable(offset_PT, pointer_PMLE4, pointer_PDPE, pointer_PD, 0, 1 );	
			if(virtual_walk && !nested)
			{
				if(utopia_enabled){

					MemoryManager* mmanager = (MemoryManager* ) (Sim()->getCoreManager()->getCoreFromID(core_id)->getMemoryManager());
					utopia_result = mmanager->accessUtopiaSubsystem(lock_signal,address_PT, data_buf, data_length, modeled, count);
					
					if(utopia_result.hitwhere == TranslationHitWhere::UTR_MISS){
						   Utopia *m_utopia;
						   UTR* m_utr;
						   m_utopia = Sim()->getUtopia();
   						   m_utr = m_utopia->getUtr(0);
						   SubsecondTime t_start = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
						   m_utr->allocate(address_PT,t_start,core_id);
						   utopia_misses++;
						   step_walk_virtual(address_PT, CR3, 0, 0, 0, 4, false, true);
					}
					else{
						   latency += utopia_result.latency;
						   utopia_hits++;
					}
				}
				else  step_walk_virtual(address_PT, CR3, 0, 0, 0, 4, false, true);
			}
			perform_pagetable_access(address_PT,1, !virtual_walk);	
			return ;
		}
	};

}