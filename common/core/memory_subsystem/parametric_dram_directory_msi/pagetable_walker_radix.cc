#include "pagetable_walker_radix.h"
#include "cache_cntlr.h"
#include "pwc.h"
#include "subsecond_time.h"
#include <math.h> 
#include <fstream>
#include <stdlib.h>
#include <time.h> 
#include "merkletree.h"
#include "page_table_walker_types.h"
#include "hit_where.h"
using namespace PtwTypes;

namespace ParametricDramDirectoryMSI{

    PageTableWalkerRadix::PageTableWalkerRadix(int number_of_levels, 
                                                   Core* _core, ShmemPerfModel* _m_shmem_perf_model, 
                                                   int *level_bit_indices,int *level_percentages, 
                                                   PWC* pwc, bool pwc_enabled,UtopiaCache* _shadow_cache, 
                                                   verify_struct *verify_metadata)
    :PageTableWalker(_core->getId(), 0, _m_shmem_perf_model, pwc, pwc_enabled)
    {
        if(level_bit_indices!=NULL){
            this->shadow_cache=_shadow_cache;
            this->core =_core;
            this->m_shmem_perf_model=_m_shmem_perf_model;
            this->stats_radix.number_of_levels=number_of_levels;
            this->stats_radix.address_bit_indices=(int *)malloc((number_of_levels+1)*sizeof(int));
            this->stats_radix.hit_percentages=(int *)malloc((number_of_levels+1)*sizeof(int));
            this->stats_radix.verify_metadata = verify_metadata;
            
            // this->stats_radix.integrity_enabled = integrity_enabled;
            // this->stats_radix.enc_enabled = enc_enabled;
            // this->stats_radix.integrity_level = integrity_level;
            for (int i = 0; i < number_of_levels+1; i++)
            {
                if(i<number_of_levels){
                    this->stats_radix.hit_percentages[i]=level_percentages[i];
                }
                this->stats_radix.address_bit_indices[i]=level_bit_indices[i];
            }
            this->starting_table=InitiateTablePtw((int)pow(2.0,(float)level_bit_indices[0]), verify_metadata);
        }
        srand(time(NULL));

        latency_per_level = new SubsecondTime[number_of_levels];
        String name = "ptw_radix";
        for (int i = 0; i < number_of_levels+1; i++){
            String metric_name = "page_level_latency_";
            String metric = metric_name+std::to_string(i).c_str();
            registerStatsMetric(name, core_id, metric, &latency_per_level[i]);        
        }

        
    }

    SubsecondTime PageTableWalkerRadix::init_walk(IntPtr address,
        UtopiaCache* shadow_cache,
        CacheCntlr *_cache,
        Core::lock_signal_t lock_signal,
        Byte* data_buf, UInt32 data_length,
        bool modeled, bool count){

            cache = _cache;
            stats.page_walks++;

            uint64_t a1;
            int shift_bits=0;
            SubsecondTime total_latency;
            SubsecondTime t_start;
            SubsecondTime now = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);


            for (int i = stats_radix.number_of_levels; i >= 1; i--)
            {
                shift_bits+=stats_radix.address_bit_indices[i];
            }
            a1=((address>>shift_bits))&0x1ff;

            if(page_walk_cache_enabled){ //@kanellok access page walk caches 

			    PWC::where_t pwc_where;
			    bool pwc_hit = false;

			    if(page_walk_cache_enabled)
                {
                    IntPtr pwc_address = (IntPtr)(&starting_table->entries[a1]);
                    pwc_where = pwc->lookup(pwc_address, t_start ,true, 1, count);
                    if( pwc_where == PWC::HIT ) pwc_hit = true; 

                }

		
                if(pwc_hit == true){

                    total_latency = pwc->access_latency.getLatency(); 

                }
                else{
                    
                    t_start = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
                    
                    IntPtr cache_address = ((IntPtr)(&starting_table->entries[a1])) & (~((64 - 1))); 

                    cache->processMemOpFromCore(
                        lock_signal,
                        Core::mem_op_t::READ,
                        cache_address, 0,
                        data_buf, data_length,
                        modeled,
                        count, CacheBlockInfo::block_type_t::PAGE_TABLE, SubsecondTime::Zero(),shadow_cache);

                    SubsecondTime t_end = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
                    total_latency = t_end - t_start + pwc->miss_latency.getLatency();
                    
                    m_shmem_perf_model->setElapsedTime(ShmemPerfModel::_USER_THREAD,now);


                    if(nuca){

                        nuca->markTranslationMetadata((IntPtr)(&starting_table->entries[a1]),CacheBlockInfo::block_type_t::PAGE_TABLE);
                        mem_manager->getCache(MemComponent::component_t::L1_DCACHE)->markMetadata((IntPtr)(&starting_table->entries[a1]),CacheBlockInfo::block_type_t::PAGE_TABLE);
                        mem_manager->getCache(MemComponent::component_t::L2_CACHE)->markMetadata((IntPtr)(&starting_table->entries[a1]),CacheBlockInfo::block_type_t::PAGE_TABLE);
                    }

                    latency_per_level[0] += total_latency;
                    
                }
		}
            uint64_t *addrs = (uint64_t *) malloc(TREE_LEVELS*sizeof(uint64_t));
            SubsecondTime *addrs_latency = (SubsecondTime *) malloc(TREE_LEVELS*sizeof(SubsecondTime));
            uint8_t level_nodes;
            SubsecondTime verify_latency = SubsecondTime::Zero();
            SubsecondTime t_end;
            bool isverify = true;
            uint64_t data;
            SubsecondTime period = SubsecondTime::SEC() / (2.6*1000000000);
            // Cache* l1dcache = mem_manager->getCache(MemComponent::component_t::L1_DCACHE);
            UInt64 cycles;
            // SubsecondTime now;
            now = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
            // printf("entry type is: %d\n",starting_table->entries[a1].entry_type);
            // uint8_t size = Sim()->getCfg()->getInt("perf_model/ptw_radix/mt_node_size");

            if(starting_table->entries[a1].entry_type==ptw_table_entry_type::PTW_NONE){
                // printf("PTW_NONE in level %d\n", 1);
                starting_table->entries[a1]=*CreateNewPtwEntryAtLevel(1,stats_radix.number_of_levels,stats_radix.address_bit_indices,stats_radix.hit_percentages,this, this->stats_radix.verify_metadata);
                if (this->stats_radix.verify_metadata->verify_typ == VERIFY_MT){
                    if (this->stats_radix.verify_metadata->mt_verify_gran == PTE_LEVEL){
                        total_latency += GetMTLatency(starting_table->entries[a1].mt_pte, 2); 
                    } else if (this->stats_radix.verify_metadata->mt_verify_gran == PT_LEVEL){
                        total_latency += GetMTLatency(starting_table->mt_pt, 2);
                    } else if (this->stats_radix.verify_metadata->mt_verify_gran == CL_LEVEL){
                        for (int i = 0; i < this->stats_radix.verify_metadata->total_size; i++){
                            total_latency += GetMTLatency(starting_table->mt_cl[i], 2);
                        }
                    }
                }
            } else {
                bool is_init_verify = false;
                IntPtr cache_data_addr;

                if (stats_radix.verify_metadata->verify_typ == VERIFY_MT){
                    // @HONG: Fix it for cache line case or 
                    if (stats_radix.verify_metadata->mt_verify_gran == PTE_LEVEL){
                        if(starting_table->entries[a1].entry_type==ptw_table_entry_type::PTW_ADDRESS){
                            //std::cout<<std::hex<<address<<" - "<<std::hex<<a1<<" - "<<level<<" Address\n";
                            latency_per_level[0] += total_latency;
                            // std::memcpy(&data, &(starting_table->entries[a1].phy_addr), sizeof(uint64_t));
                            data = starting_table->entries[a1].phy_addr;

                            cache_data_addr = ((IntPtr)(data)) & (~((64 - 1)));

                            HitWhere::where_t hit_where = cache->processMemOpFromCore(
                                lock_signal,
                                Core::mem_op_t::READ,
                                cache_data_addr, 0,
                                data_buf, data_length,
                                modeled,
                                count, CacheBlockInfo::block_type_t::PAGE_TABLE, SubsecondTime::Zero());

                            is_init_verify = isInitVerify(hit_where, 0, stats_radix.verify_metadata);

                            if (is_init_verify){
                                isverify = verify_mt(starting_table->entries[a1].mt_pte, &data, stats_radix.verify_metadata, addrs, &level_nodes, NULL);
                                // printf("------------verification started in physical address!\n");
                                // if (!isverify){
                                //     printf("Merkle tree verification failed in the layer %d\n", 1);
                                // } else{
                                //     printf("Merkle tree verification succedded in the layer %d\n", 1);
                                // }
                                // verify_mtfy_latency = 0;

                                // TODO: Keep the t_start and reset the getElapsedTime
                                IntPtr cache_address;
                                for (int i = 0; i < level_nodes; i++){
                                    cache_address = ((IntPtr)(addrs[i])) & (~((64 - 1))); 
                                    // printf("Address to be visited: %llu\n", cache_address);
                                    t_start= getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
                                    HitWhere::where_t hit_where = cache->processMemOpFromCore(
                                        lock_signal,
                                        Core::mem_op_t::READ,
                                        cache_address, 0,
                                        data_buf, data_length,
                                        modeled,
                                        count, CacheBlockInfo::block_type_t::PAGE_TABLE, SubsecondTime::Zero());
                                    t_end = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
                                    // printf("Hit: %d\n", hit_where);
                                    
                                    verify_latency += t_end - t_start;
                                    addrs_latency[i] = t_end - t_start;
                                    cycles = SubsecondTime::divideRounded(addrs_latency[i], period);
                                    // printf("The number of cycles is %llu\n", cycles);

                                }
                                verify_latency += GetVerifyLatency(starting_table->entries[a1].mt_pte, 2);
                            }
                            
                        // total_latency = t_end - t_start + pwc->miss_latency.getLatency();
                        }
                    } else if (stats_radix.verify_metadata->mt_verify_gran == PT_LEVEL){
                        uint64_t *data_table = (uint64_t*) malloc(stats_radix.verify_metadata->total_entry*sizeof(uint64_t));
                        for (int i = 0; i < stats_radix.verify_metadata->total_entry; i++){
                            data_table[i] = (uint64_t) &starting_table->entries[i];
                        }
                        uint32_t offset = std::rand() % (stats_radix.verify_metadata->n_partition); // find a random node to be verified
                        uint32_t data_idx, bit_idx;
                        bit_idx = offset % (stats_radix.verify_metadata->addr_len/stats_radix.verify_metadata->mt_node_size);
                        data_idx = (offset - bit_idx) * stats_radix.verify_metadata->mt_node_size/stats_radix.verify_metadata->addr_len;

                        cache_data_addr = ((IntPtr)(&data_table[data_idx])) & (~((64 - 1)));
                         

                        HitWhere::where_t hit_where = cache->processMemOpFromCore(
                            lock_signal,
                            Core::mem_op_t::READ,
                            cache_data_addr, 0,
                            data_buf, data_length,
                            modeled,
                            count, CacheBlockInfo::block_type_t::PAGE_TABLE, SubsecondTime::Zero());

                        is_init_verify = isInitVerify(hit_where, 0, stats_radix.verify_metadata);
    

                        if (is_init_verify){
                            isverify = verify_mt(starting_table->mt_pt, data_table, stats_radix.verify_metadata, addrs, &level_nodes, offset);
                            IntPtr cache_address;
                            for (int i = 0; i < level_nodes; i++){
                                cache_address = ((IntPtr)(addrs[i])) & (~((64 - 1))); 
                                // printf("Address to be visited: %llu\n", cache_address);
                                t_start= getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
                                HitWhere::where_t hit_where = cache->processMemOpFromCore(
                                    lock_signal,
                                    Core::mem_op_t::READ,
                                    cache_address, 0,
                                    data_buf, data_length,
                                    modeled,
                                    count, CacheBlockInfo::block_type_t::PAGE_TABLE, SubsecondTime::Zero());
                                t_end = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
                                // printf("Hit: %d\n", hit_where);
                                
                                verify_latency += t_end - t_start;
                                addrs_latency[i] = t_end - t_start;
                                cycles = SubsecondTime::divideRounded(addrs_latency[i], period);
                                // printf("The number of cycles is %llu\n", cycles);

                            }
                            verify_latency += GetVerifyLatency(starting_table->mt_pt, 2);
                        }
                        
                    } else if (stats_radix.verify_metadata->mt_verify_gran == CL_LEVEL){
                        uint64_t *data_table = (uint64_t*) malloc(stats_radix.verify_metadata->total_entry*sizeof(uint64_t));
                        for (int i = 0; i < stats_radix.verify_metadata->table_size; i++){
                            data_table[i] = (uint64_t) &starting_table->entries[i*stats_radix.verify_metadata->cl_size];
                        }
                        uint32_t offset = std::rand() % (stats_radix.verify_metadata->n_partition); // find a random node to be verified
                        uint32_t data_idx, bit_idx;
                        bit_idx = offset % (stats_radix.verify_metadata->addr_len/stats_radix.verify_metadata->mt_node_size);
                        data_idx = (offset - bit_idx) * stats_radix.verify_metadata->mt_node_size/stats_radix.verify_metadata->addr_len;

                        cache_data_addr = ((IntPtr)(data_table[data_idx])) & (~((64 - 1)));
                    
                        HitWhere::where_t hit_where = cache->processMemOpFromCore(
                            lock_signal,
                            Core::mem_op_t::READ,
                            cache_data_addr, 0,
                            data_buf, data_length,
                            modeled,
                            count, CacheBlockInfo::block_type_t::PAGE_TABLE, SubsecondTime::Zero());

                        is_init_verify = isInitVerify(hit_where, 0, stats_radix.verify_metadata);

                        if (is_init_verify){
                            isverify = verify_mt(starting_table->mt_cl[data_idx/stats_radix.verify_metadata->cl_size], data_table, stats_radix.verify_metadata, addrs, &level_nodes, offset);
                            IntPtr cache_address;
                            for (int i = 0; i < level_nodes; i++){
                                cache_address = ((IntPtr)(addrs[i])) & (~((64 - 1))); 
                                // printf("Address to be visited: %llu\n", cache_address);
                                t_start= getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
                                HitWhere::where_t hit_where = cache->processMemOpFromCore(
                                    lock_signal,
                                    Core::mem_op_t::READ,
                                    cache_address, 0,
                                    data_buf, data_length,
                                    modeled,
                                    count, CacheBlockInfo::block_type_t::PAGE_TABLE, SubsecondTime::Zero());
                                t_end = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
                                // printf("Hit: %d\n", hit_where);
                                
                                verify_latency += t_end - t_start;
                                addrs_latency[i] = t_end - t_start;
                                cycles = SubsecondTime::divideRounded(addrs_latency[i], period);
                                // printf("The number of cycles is %llu\n", cycles);

                            }
                            verify_latency += GetVerifyLatency(starting_table->mt_pt, 2);
                        }
                    }
            
                    // latency_per_level[0] += total_latency;
                }
                return total_latency + verify_latency; 

            }

            SubsecondTime final_latency = total_latency+InitializeWalkRecursive(address,2,starting_table->entries[a1].next_level_table,lock_signal,data_buf,data_length, modeled, count) + verify_latency;
            
            m_shmem_perf_model->setElapsedTime(ShmemPerfModel::_USER_THREAD,now);
            UInt64 vpn = address >> init_walk_functional(address);
            track_per_page_ptw_latency(vpn,final_latency);


            return final_latency;

    }

    SubsecondTime PageTableWalkerRadix::InitializeWalkRecursive(uint64_t address,
        int level,ptw_table* new_table,
        Core::lock_signal_t lock_signal,
        Byte* data_buf, UInt32 data_length,
        bool modeled, bool count){
        uint64_t a1;
        int shift_bits=0;
        IntPtr pwc_address;
        SubsecondTime t_start;
        SubsecondTime total_latency;
        
        SubsecondTime period = SubsecondTime::SEC() / (2.6*1000000000);
        // Cache* l1dcache = mem_manager->getCache(MemComponent::component_t::L1_DCACHE);
        UInt64 cycles;
        for (int i = stats_radix.number_of_levels; i >= level; i--)
        {
            shift_bits+=stats_radix.address_bit_indices[i];
        }

        a1=((address>>shift_bits))&0x1ff;
        // printf("entry type is: %d\n",new_table->entries[a1].entry_type);

        if(page_walk_cache_enabled){ //@kanellok access page walk caches 

            PWC::where_t pwc_where;
            bool pwc_hit = false;

            if(page_walk_cache_enabled && level != (stats_radix.number_of_levels) )
            {
                pwc_address = (IntPtr)(&new_table->entries[a1]);
                pwc_where = pwc->lookup(pwc_address, t_start ,true, level, count);
                if( pwc_where == PWC::HIT ) pwc_hit = true; 

            }

    
            if(pwc_hit == true){

                total_latency = pwc->access_latency.getLatency(); 

            }
            else{
                
                t_start = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
                
                IntPtr cache_address = ((IntPtr)(&new_table->entries[a1])) & (~((64 - 1))); 

                cache->processMemOpFromCore(
                    lock_signal,
                    Core::mem_op_t::READ,
                    cache_address, 0,
                    data_buf, data_length,
                    modeled,
                    count, CacheBlockInfo::block_type_t::PAGE_TABLE, SubsecondTime::Zero());

                SubsecondTime t_end = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
                total_latency = t_end - t_start + pwc->miss_latency.getLatency();
                


                if(nuca){

                    nuca->markTranslationMetadata((IntPtr)(&new_table->entries[a1]),CacheBlockInfo::block_type_t::PAGE_TABLE);
                    mem_manager->getCache(MemComponent::component_t::L1_DCACHE)->markMetadata((IntPtr)(&new_table->entries[a1]),CacheBlockInfo::block_type_t::PAGE_TABLE);
                    mem_manager->getCache(MemComponent::component_t::L2_CACHE)->markMetadata((IntPtr)(&new_table->entries[a1]),CacheBlockInfo::block_type_t::PAGE_TABLE);

                }

                latency_per_level[level-1] += total_latency;
                
            }
    }
        uint64_t *addrs = (uint64_t*) malloc(TREE_LEVELS*sizeof(uint64_t));
        SubsecondTime *addrs_latency = (SubsecondTime *) malloc(TREE_LEVELS*sizeof(SubsecondTime));

        uint8_t level_nodes;
        SubsecondTime verify_latency = SubsecondTime::Zero();
        SubsecondTime t_end;
        bool isverify = true;
        uint64_t data;
        // Cache* l1dcache = mem_manager->getCache(MemComponent::component_t::L1_DCACHE);
        uint8_t size = Sim()->getCfg()->getInt("perf_model/ptw_radix/mt_node_size");
        if(new_table->entries[a1].entry_type==ptw_table_entry_type::PTW_NONE){
            // printf("PTW_NONE in level %d\n", level);
            new_table->entries[a1]=*CreateNewPtwEntryAtLevel(level,stats_radix.number_of_levels,stats_radix.address_bit_indices,stats_radix.hit_percentages,this, this->stats_radix.verify_metadata);
            if (this->stats_radix.verify_metadata->mt_verify_gran == PTE_LEVEL){
                total_latency += GetMTLatency(new_table->entries[a1].mt_pte, 2); 
            } else if (this->stats_radix.verify_metadata->mt_verify_gran == PT_LEVEL){
                total_latency += GetMTLatency(new_table->mt_pt, 2);
            } else if (this->stats_radix.verify_metadata->mt_verify_gran == CL_LEVEL){
                for (int i = 0; i < this->stats_radix.verify_metadata->total_size; i++){
                    total_latency += GetMTLatency(new_table->mt_cl[i], 2);
                }
            }
        }
        else {
            bool is_init_verify = false;
            IntPtr cache_data_addr;

            if (stats_radix.verify_metadata->verify_typ == VERIFY_MT){
                // @HONG: Fix it for cache line case or 
                if (stats_radix.verify_metadata->mt_verify_gran == PTE_LEVEL){
                    if(new_table->entries[a1].entry_type==ptw_table_entry_type::PTW_ADDRESS){
                        //std::cout<<std::hex<<address<<" - "<<std::hex<<a1<<" - "<<level<<" Address\n";
                        latency_per_level[0] += total_latency;
                        // std::memcpy(&data, &(starting_table->entries[a1].phy_addr), sizeof(uint64_t));
                        data = new_table->entries[a1].phy_addr;

                        cache_data_addr = ((IntPtr)(data)) & (~((64 - 1)));

                        HitWhere::where_t hit_where = cache->processMemOpFromCore(
                            lock_signal,
                            Core::mem_op_t::READ,
                            cache_data_addr, 0,
                            data_buf, data_length,
                            modeled,
                            count, CacheBlockInfo::block_type_t::PAGE_TABLE, SubsecondTime::Zero());

                        is_init_verify = isInitVerify(hit_where, level, stats_radix.verify_metadata);

                        if (is_init_verify){
                            isverify = verify_mt(new_table->entries[a1].mt_pte, &data, stats_radix.verify_metadata, addrs, &level_nodes, NULL);
                            // printf("------------verification started in physical address!\n");
                            // if (!isverify){
                            //     printf("Merkle tree verification failed in the layer %d\n", 1);
                            // } else{
                            //     printf("Merkle tree verification succedded in the layer %d\n", 1);
                            // }
                            // verify_mtfy_latency = 0;

                            // TODO: Keep the t_start and reset the getElapsedTime
                            IntPtr cache_address;
                            for (int i = 0; i < level_nodes; i++){
                                cache_address = ((IntPtr)(addrs[i])) & (~((64 - 1))); 
                                // printf("Address to be visited: %llu\n", cache_address);
                                t_start= getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
                                HitWhere::where_t hit_where = cache->processMemOpFromCore(
                                    lock_signal,
                                    Core::mem_op_t::READ,
                                    cache_address, 0,
                                    data_buf, data_length,
                                    modeled,
                                    count, CacheBlockInfo::block_type_t::PAGE_TABLE, SubsecondTime::Zero());
                                t_end = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
                                // printf("Hit: %d\n", hit_where);
                                
                                verify_latency += t_end - t_start;
                                addrs_latency[i] = t_end - t_start;
                                cycles = SubsecondTime::divideRounded(addrs_latency[i], period);
                                // printf("The number of cycles is %llu\n", cycles);

                            }
                            verify_latency += GetVerifyLatency(new_table->entries[a1].mt_pte, 2);
                        }
                    // total_latency = t_end - t_start + pwc->miss_latency.getLatency();
                    }
                } else if (stats_radix.verify_metadata->mt_verify_gran == PT_LEVEL){
                    uint64_t *data_table = (uint64_t*) malloc(stats_radix.verify_metadata->total_entry*sizeof(uint64_t));
                    for (int i = 0; i < stats_radix.verify_metadata->total_entry; i++){
                        data_table[i] = (uint64_t) &new_table->entries[i];
                    }
                    uint32_t offset = std::rand() % (stats_radix.verify_metadata->n_partition); // find a random node to be verified
                    uint32_t data_idx, bit_idx;
                    bit_idx = offset % (stats_radix.verify_metadata->addr_len/stats_radix.verify_metadata->mt_node_size);
                    data_idx = (offset - bit_idx) * stats_radix.verify_metadata->mt_node_size/stats_radix.verify_metadata->addr_len;

                    cache_data_addr = ((IntPtr)(&data_table[data_idx])) & (~((64 - 1)));

                    HitWhere::where_t hit_where = cache->processMemOpFromCore(
                        lock_signal,
                        Core::mem_op_t::READ,
                        cache_data_addr, 0,
                        data_buf, data_length,
                        modeled,
                        count, CacheBlockInfo::block_type_t::PAGE_TABLE, SubsecondTime::Zero());

                    is_init_verify = isInitVerify(hit_where, level, stats_radix.verify_metadata);

                    if (is_init_verify){
                        isverify = verify_mt(new_table->mt_pt, data_table, stats_radix.verify_metadata, addrs, &level_nodes, offset);
                        IntPtr cache_address;
                        for (int i = 0; i < level_nodes; i++){
                            cache_address = ((IntPtr)(addrs[i])) & (~((64 - 1))); 
                            // printf("Address to be visited: %llu\n", cache_address);
                            t_start= getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
                            HitWhere::where_t hit_where = cache->processMemOpFromCore(
                                lock_signal,
                                Core::mem_op_t::READ,
                                cache_address, 0,
                                data_buf, data_length,
                                modeled,
                                count, CacheBlockInfo::block_type_t::PAGE_TABLE, SubsecondTime::Zero());
                            t_end = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
                            // printf("Hit: %d\n", hit_where);
                            
                            verify_latency += t_end - t_start;
                            addrs_latency[i] = t_end - t_start;
                            cycles = SubsecondTime::divideRounded(addrs_latency[i], period);
                            // printf("The number of cycles is %llu\n", cycles);
                        }
                        verify_latency += GetVerifyLatency(new_table->mt_pt, 2);
                    }
                } else if (stats_radix.verify_metadata->mt_verify_gran == CL_LEVEL){
                    uint64_t *data_table = (uint64_t*) malloc(stats_radix.verify_metadata->total_entry*sizeof(uint64_t));
                    for (int i = 0; i < stats_radix.verify_metadata->table_size; i++){
                        data_table[i] = (uint64_t) &new_table->entries[i*stats_radix.verify_metadata->cl_size];
                    }
                    uint32_t offset = std::rand() % (stats_radix.verify_metadata->n_partition); // find a random node to be verified
                    uint32_t data_idx, bit_idx;
                    bit_idx = offset % (stats_radix.verify_metadata->addr_len/stats_radix.verify_metadata->mt_node_size);
                    data_idx = (offset - bit_idx) * stats_radix.verify_metadata->mt_node_size/stats_radix.verify_metadata->addr_len;

                    cache_data_addr = ((IntPtr)(data_table[data_idx])) & (~((64 - 1)));

                    HitWhere::where_t hit_where = cache->processMemOpFromCore(
                        lock_signal,
                        Core::mem_op_t::READ,
                        cache_data_addr, 0,
                        data_buf, data_length,
                        modeled,
                        count, CacheBlockInfo::block_type_t::PAGE_TABLE, SubsecondTime::Zero());

                    is_init_verify = isInitVerify(hit_where, level, stats_radix.verify_metadata);
                    
                    if (is_init_verify){
                        isverify = verify_mt(new_table->mt_cl[data_idx], data_table, stats_radix.verify_metadata, addrs, &level_nodes, offset);
                        IntPtr cache_address;
                        for (int i = 0; i < level_nodes; i++){
                            cache_address = ((IntPtr)(addrs[i])) & (~((64 - 1))); 
                            // printf("Address to be visited: %llu\n", cache_address);
                            t_start= getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
                            HitWhere::where_t hit_where = cache->processMemOpFromCore(
                                lock_signal,
                                Core::mem_op_t::READ,
                                cache_address, 0,
                                data_buf, data_length,
                                modeled,
                                count, CacheBlockInfo::block_type_t::PAGE_TABLE, SubsecondTime::Zero());
                            t_end = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
                            // printf("Hit: %d\n", hit_where);
                            
                            verify_latency += t_end - t_start;
                            addrs_latency[i] = t_end - t_start;
                            cycles = SubsecondTime::divideRounded(addrs_latency[i], period);
                            // printf("The number of cycles is %llu\n", cycles);

                        }
                        verify_latency += GetVerifyLatency(new_table->mt_pt, 2);
                    }
                }
            }
            // if(new_table->entries[a1].entry_type==ptw_table_entry_type::PTW_ADDRESS){
            //     //std::cout<<std::hex<<address<<" - "<<std::hex<<a1<<" - "<<level<<" Address\n";
            //     // @HONG: add later the latency from using addrs
            //     // std::memcpy(&data, &(starting_table->entries[a1].phy_addr), sizeof(uint64_t));
            //     if (stats_radix.integrity_enabled){
            //         data = (uint64_t) new_table->entries[a1].phy_addr;
            //         // printf("------------verification started in physical address in layer %u!\n", level);

            //         isverify = verify_mt(new_table->entries[a1].mt, data, size, 64, addrs, &level_nodes);
            //         // if (!isverify){
            //         //     printf("Merkle tree verification failed in the layer %u\n", level);
            //         // }else{
            //         //     printf("Merkle tree verification succedded in the layer %u\n", level);
            //         // }
            //         // verify_latency = 0;
            //         IntPtr cache_address;

            //         // TODO: Keep the t_start and reset the getElapsedTime
            //         for (int i = 0; i < level_nodes; i++){
            //             cache_address = ((IntPtr)(addrs[i])) & (~((64 - 1))); 
            //             // printf("Address to be visited: %llu\n", cache_address);

            //             t_start= getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
            //             HitWhere::where_t hit_where = cache->processMemOpFromCore(
            //                 lock_signal,
            //                 Core::mem_op_t::READ,
            //                 cache_address, 0,
            //                 data_buf, data_length,
            //                 modeled,
            //                 count, CacheBlockInfo::block_type_t::PAGE_TABLE, SubsecondTime::Zero());
            //             t_end = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
            //             // printf("Hit: %d\n", hit_where);
            //             verify_latency += t_end - t_start;
            //             addrs_latency[i] = t_end - t_start;
            //             cycles = SubsecondTime::divideRounded(addrs_latency[i], period);
            //             // printf("The number of cycles is %llu\n", cycles);

            //         }
            //         verify_latency += GetVerifyLatency(new_table->entries[a1].mt,2);

            //     } 

            //     return total_latency + verify_latency;
            // }
            // // std::memcpy(&data, starting_table->entries[a1].next_level_table, sizeof(ptw_table));
            // if (stats_radix.integrity_enabled){

            //     data = (uint64_t) new_table->entries[a1].next_level_table;
            //     // printf("------------verification started in next level table in layer %u!\n", level);

            //     isverify = verify_mt(new_table->entries[a1].mt, data, size, 64, addrs, &level_nodes);
            //     // if (!isverify){
            //     //     printf("Merkle tree verification failed in the layer %u\n", level);
            //     // }else{
            //     //     printf("Merkle tree verification succedded in the layer %u\n", level);
            //     // }
            //     IntPtr cache_address;

            //     // TODO: Keep the t_start and reset the getElapsedTime
            //     for (int i = 0; i < level_nodes; i++){
            //         cache_address = ((IntPtr)(addrs[i]  )) & (~((64 - 1))); 
            //     // printf("Address to be visited: %llu\n", cache_address);

            //         t_start= getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
            //         HitWhere::where_t hit_where = cache->processMemOpFromCore(
            //             lock_signal,
            //             Core::mem_op_t::READ,
            //             cache_address, 0,
            //             data_buf, data_length,
            //             modeled,
            //             count, CacheBlockInfo::block_type_t::PAGE_TABLE, SubsecondTime::Zero());
            //         t_end = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
            //         // printf("Hit: %d\n", hit_where);

            //         verify_latency += t_end - t_start;  
            //         addrs_latency[i] = t_end - t_start;
            //         cycles = SubsecondTime::divideRounded(addrs_latency[i], period);
            //         // printf("The number of cycles is %llu\n", cycles);
            //     }
            //     verify_latency += GetVerifyLatency(new_table->entries[a1].mt,2);

            // } 


        }
        return total_latency+InitializeWalkRecursive(address,level+1,new_table->entries[a1].next_level_table,lock_signal,data_buf,data_length,modeled,count) + verify_latency;
    }
    
    int PageTableWalkerRadix::init_walk_functional(IntPtr address){
        uint64_t a1;
        int shift_bits=0;
        int isverify = true;
        //std::cout << "Address  = " << address << "Number of levels" << stats_radix.number_of_levels << std::endl;
        
        for (int i = stats_radix.number_of_levels; i >= 1; i--)
        {
            shift_bits+=stats_radix.address_bit_indices[i];
        }
        a1=((address>>shift_bits))&0x1ff;
        // std::cout << "init_walk_functional called " << std::endl;
        // new address
            // uint8_t size = Sim()->getCfg()->getInt("perf_model/ptw_radix/mt_node_size");

        if(starting_table->entries[a1].entry_type==ptw_table_entry_type::PTW_NONE){

            starting_table->entries[a1]=*CreateNewPtwEntryAtLevel(1,stats_radix.number_of_levels,stats_radix.address_bit_indices,stats_radix.hit_percentages,this, this->stats_radix.verify_metadata);
            //isverify = verify_mt()        
        }
        uint64_t addrs[TREE_LEVELS];
        uint8_t level_nodes;
        uint64_t data;

        // printf("functional mt \n");
        // mt_print(starting_table->entries[a1].mt);
        if(starting_table->entries[a1].entry_type==ptw_table_entry_type::PTW_ADDRESS){
            
                int page_size=0;
                for(int i=stats_radix.number_of_levels;i>0;i--){
                    page_size+=stats_radix.address_bit_indices[i];
                }
                // std::memcpy(&data, &(starting_table->entries[a1].phy_addr), sizeof(uint64_t));
                // data = starting_table->entries[a1].phy_addr;
                // isverify = verify_mt(starting_table->entries[a1].mt, data, 16, 64, addrs, &level_nodes);
                // if (!isverify){
                //     printf("Merkle tree verification failed in the layer %d \n", level_nodes);
                // } else{
                //     printf("verificaiton succeeded\n");
                // }
                return (int)(page_size);
        }  
        // std::memcpy(&data, starting_table->entries[a1].next_level_table, sizeof(uint64_t));
        // data = (uint64_t) starting_table->entries[a1].next_level_table;

        // isverify = verify_mt(starting_table->entries[a1].mt, data, 16, 64, addrs, &level_nodes);
        // if (!isverify){
        //     printf("Merkle tree verification failed in the layer %d \n", level_nodes);
        // }else{
        //             printf("verificaiton succeeded\n");
        //         }
        return init_walk_recursive_functional(address,2,starting_table->entries[a1].next_level_table);
    }

    int PageTableWalkerRadix::init_walk_recursive_functional(uint64_t address,int level,ptw_table* new_table){
        uint64_t a1;
        int shift_bits=0;
        // bool isverify = true;
        // uint8_t level_nodes;
       // std::cout << "Level  = " << level << std::endl;
        for (int i = stats_radix.number_of_levels; i >= level; i--)
        {
            shift_bits+=stats_radix.address_bit_indices[i];
        }
        a1=((address>>shift_bits))&0x1ff;
        // std::cout << "Physical address = " << new_table->entries[a1].phy_addr << std::endl;
        // std::cout << "EntryType = " << new_table->entries[a1].entry_type << std::endl;
        // std::cout << "init_walk_recursive_functional called " << std::endl;

        if(new_table->entries[a1].entry_type==ptw_table_entry_type::PTW_NONE){
            // uint8_t size = Sim()->getCfg()->getInt("perf_model/ptw_radix/mt_node_size");

            new_table->entries[a1]=*CreateNewPtwEntryAtLevel(level,stats_radix.number_of_levels,stats_radix.address_bit_indices,stats_radix.hit_percentages,this, this->stats_radix.verify_metadata);
        }
        // uint64_t addrs[TREE_LEVELS];
        // uint64_t data;
        // uint8_t level;
        // SubsecondTime verify_latency;
        // SubsecondTime t_end;

        if(new_table->entries[a1].entry_type==ptw_table_entry_type::PTW_ADDRESS){
            int page_size=0;
            for(int i=stats_radix.number_of_levels;i>level-1;i--){
                page_size+=stats_radix.address_bit_indices[i];
            }
            // std::memcpy(&data, &(starting_table->entries[a1].phy_addr), sizeof(uint64_t));
            // data = (uint64_t) starting_table->entries[a1].phy_addr;

            // isverify = verify_mt(new_table->entries[a1].mt, data, 16, 64, addrs, &level_nodes) ;
            // if (!isverify){
            //     printf("Merkle tree verification failed in the layer %d \n", level_nodes);
            // }else{
            //         printf("verificaiton succeeded\n");
            //     }
            return (int)(page_size);
        }
        // std::memcpy(&data, starting_table->entries[a1].next_level_table, sizeof(uint64_t));
            // data = (uint64_t) starting_table->entries[a1].next_level_table;

        // isverify = verify_mt(new_table->entries[a1].mt, data, 16, 64, addrs, &level_nodes)  ;
        // if (!isverify){
        //     printf("Merkle tree verification failed in the layer %d \n", level_nodes);
        // }else{
        //             printf("verificaiton succeeded\n");
        //         }
        return init_walk_recursive_functional(address,level+1,new_table->entries[a1].next_level_table);
    }

    // bool PageTableWalkerRadix::isPageIntegral(IntPtr address){

    // }
    bool PageTableWalkerRadix::isPageFault(IntPtr address){
        uint64_t a1;
        int shift_bits=0;
        //std::cout << "Address  = " << address << "Number of levels" << stats_radix.number_of_levels << std::endl;
        
        for (int i = stats_radix.number_of_levels; i >= 1; i--)
        {
            shift_bits+=stats_radix.address_bit_indices[i];
        }
        a1=((address>>shift_bits))&0x1ff;

        if(starting_table->entries[a1].entry_type==ptw_table_entry_type::PTW_NONE){
            return true;
        }
        if(starting_table->entries[a1].entry_type==ptw_table_entry_type::PTW_ADDRESS){
            return false;
        }

        return isPageFaultHelper(address,2,starting_table->entries[a1].next_level_table);


    }

    bool PageTableWalkerRadix::isPageFaultHelper(uint64_t address,int level,ptw_table* new_table){

        uint64_t a1;
        int shift_bits=0;
       // std::cout << "Level  = " << level << std::endl;
        for (int i = stats_radix.number_of_levels; i >= level; i--)
        {
            shift_bits+=stats_radix.address_bit_indices[i];
        }
        a1=((address>>shift_bits))&0x1ff;

       // std::cout << "EntryType = " << new_table->entries[a1].entry_type << std::endl;

        if(new_table->entries[a1].entry_type==ptw_table_entry_type::PTW_NONE){
            return true;
        }
        if(new_table->entries[a1].entry_type==ptw_table_entry_type::PTW_ADDRESS){

            return false;
            
        }
        return isPageFaultHelper(address,level+1,new_table->entries[a1].next_level_table);

    }

    /** dsafsdf
     * 
    */
    bool PageTableWalkerRadix::isInitVerify(HitWhere::where_t hit_where, int level, verify_struct* verify_metadata){
        if (hit_where == HitWhere::L1I || hit_where == HitWhere::L1_OWN){
            if (verify_metadata->mt_int_level >= L1){
                if (verify_metadata->mt_verify_level >= level){
                    return true;
                }
            }
        } else if (hit_where == HitWhere::L2_OWN){
            if (verify_metadata->mt_int_level >= L2){
                if (verify_metadata->mt_verify_level >= level){
                    return true;
                }
            }
        } else if (hit_where == HitWhere::L3_OWN){
            if (verify_metadata->mt_int_level >= L3){
                if (verify_metadata->mt_verify_level >= level){
                    return true;
                }            
            }
        } else if (hit_where == HitWhere::DRAM){
            if (verify_metadata->mt_int_level == MEM){
                if (verify_metadata->mt_verify_level >= level){
                    return true;
                }            
            }
        }

        return false;
    }
}