#include "page_table_walker_types.h"
#include "pagetable_walker.h"
#include "cache_cntlr.h"
#include "utopia_cache_template.h"


#include "fixed_types.h"
#include "city.h"

#include <stdint.h>
namespace ParametricDramDirectoryMSI{
    class PageTableHashDontCache:public PageTableWalker{
        struct entry{
            bool empty;
            long vpn;
            int * ptes[4];
        };
        entry* table_small_pages;
        entry* table_big_pages;
        Core* core;
        CacheCntlr *cache;
        ShmemPerfModel* m_shmem_perf_model;
        int table_size_in_bits;
        int small_page_size_in_bits;
        int big_page_size_in_bits;
        int small_page_percentage;
        ComponentLatency latency_for_disk_access;
        public:
            int hash_function(IntPtr address);
            PageTableHashDontCache(int table_size_in_bits,int small_page_size_in_bits,int big_page_size_in_bits,int _small_page_percentage,Core* _core,ShmemPerfModel* _m_shmem_perf_model,PWC* pwc, bool pwc_enabled);
            SubsecondTime init_walk(IntPtr address, UtopiaCache* shadow_cache, CacheCntlr *cache, Core::lock_signal_t lock_signal, Byte* _data_buf, UInt32 _data_length, bool modeled, bool count) ;
            int init_walk_functional(IntPtr address);
            bool isPageFault(IntPtr address);
    };
}