#ifndef PTWCV_H
#define PTWCV_H

#include "fixed_types.h"
#include "./cuckoo/elastic_cuckoo_table.h"
#include "cache.h"
#include "pagetable_walker.h"
#include <unordered_map>
#include "cache_cntlr.h"
#include <random>
#include "pwc.h"
#include "utopia_cache_template.h"

namespace ParametricDramDirectoryMSI
{
   class PageTableWalkerCuckooVirtual: public PageTableWalker{
        private:
            elasticCuckooTable_t guestElasticCuckooHT;
            elasticCuckooTable_t hostElasticCuckooHT;

		        UInt64 utopia_hits;
		        UInt64 utopia_misses;
		        SubsecondTime latency_virtual;
            UInt64 cuckoo_faults;
            UInt64 cuckoo_hits;
            SubsecondTime cuckoo_latency;

            bool utopia_enabled;

            void accessTable(IntPtr address,bool nested);
        public:
    		    PageTableWalkerCuckooVirtual(int core_id, int _page_size, ShmemPerfModel* _m_shmem_perf_model, PWC* _pwc, 
												bool _page_walk_cache_enabled, int d, char* hash_func, int size,
												float rehash_threshold, uint8_t scale,
                    							uint8_t swaps, uint8_t priority);
            int init_walk_functional(IntPtr address){return 0;};
            SubsecondTime init_walk(IntPtr address, UtopiaCache* shadow_cache,CacheCntlr *cache, Core::lock_signal_t lock_signal, Byte* _data_buf, UInt32 _data_length, bool modeled, bool count) override;
            bool isPageFault(IntPtr address){return false;}

   };
}
   
#endif

