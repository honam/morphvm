#include "page_table_walker_types.h"
#include "merkletree.h"
#include <math.h> 
#include <stdlib.h>
#include <time.h>

// int phy_inc = 0;
uint64_t phy_addr = 0;
// uint16_t size = 16; // the number of bits for representing 1 node

using namespace ParametricDramDirectoryMSI;

namespace PtwTypes{

    verify_struct* InitiateVerifyMetadata(verification_type verify_typ, mt_verification_granularity mt_verify_gran,
                                          mt_integrity_level mt_int_level, uint16_t mt_node_size, mt_verification_level mt_verify_level){
        verify_struct* verify_metadata = (verify_struct*)malloc(sizeof(verify_struct));
        
        verify_metadata->verify_typ = verify_typ;
        verify_metadata->mt_verify_gran = mt_verify_gran;
        verify_metadata->mt_int_level = mt_int_level;
        verify_metadata->mt_node_size = mt_node_size;
        verify_metadata->mt_verify_level = mt_verify_level;
        verify_metadata->addr_len = 64; // address length is in Bits
        verify_metadata->cl_size = 64; // cache line size is in Bytes
        verify_metadata->table_size = 512; // the number of entries for 4KB page table => 64 bits x 512 entries 

        // if (verify_metadata->mt_verify_gran == PT_LEVEL){
        //     verify_metadata->total_size = 1<<15; // 4KB => 512*64 bits = 256*128 bits => 2**15 bits
        //     verify_metadata->total_entry = 512; 
        // }
        if (verify_metadata->mt_verify_gran == CL_LEVEL){
            verify_metadata->total_size = 64*8; // 64Bytes => 512 bits => 2**9 bits
            verify_metadata->total_entry = 8; // the number of 8B entries per cache line
        }
        // if (verify_metadata->mt_verify_gran == PTE_LEVEL){
        //     verify_metadata->total_size = 64; // 64 bits
        //     verify_metadata->total_entry = 1;
        // }
        verify_metadata->n_partition = verify_metadata->total_size / verify_metadata->mt_node_size; 
        printf("n_partition: %d\n", verify_metadata->n_partition);

        if (mt_node_size <= 8){
            verify_metadata->n_subpartition = 1;
        } else{
            verify_metadata->n_subpartition = mt_node_size / 8; 
        }

        
        printf("n_subpartition: %d\n", verify_metadata->n_subpartition);

        // needed if merkle_tree_node_size < addr_len
        verify_metadata->n_data_partition = verify_metadata->addr_len/verify_metadata->mt_node_size; 
        // needed if merkle_tree_node_size > addr_len
        verify_metadata->n_data_blocks = verify_metadata->mt_node_size/verify_metadata->addr_len;

        // x[0], x[1], x[2], x[3] => MT[0] = {x[3], x[2], x[1], x[0]}
        // 256 bits => 32 x 8 bit =>  

        printf("n_data_blocks: %d\n", verify_metadata->n_data_blocks);

        verify_metadata->n_cl_blocks = 64;

        // verify_metadata->n_entry_per
        // if (verify_typ == PTE_LEVEL){
        //     if (verify_metadata->mt_node_size > verify_metadata->addr_len){
        //         printf("For page table entry level verification, the size of the MT node can't be larger than the entry itself!\n");
        //     }
        // }

        return verify_metadata;
    }

    void PrintVerifyMetadata(verify_struct* verify_metadata){

        printf("Verify Metadata is printed\n");
        printf("Verify_type: %d\n", verify_metadata->verify_typ);
        printf("Verify_granularity: %d\n", verify_metadata->mt_verify_gran);
        printf("mt_integrity_level: %d\n", verify_metadata->mt_int_level);
        printf("total_size", verify_metadata->total_size);

    }

    /** 
     * Create a new page table entry
     * 
     * 
    */
    ptw_table_entry* CreateNewPtwEntryAtLevel(int level,int number_of_levels,int *level_bit_indices,int *level_percentages,PageTableWalker *ptw, verify_struct *verify_metadata){
        ptw_table_entry* e=(ptw_table_entry*)malloc(sizeof(ptw_table_entry));
        // Create page table entries sequentially
        if(level!=number_of_levels){
            int numb=rand()%100; 
            if(numb<level_percentages[level-1]){
                e->entry_type=ptw_table_entry_type::PTW_ADDRESS;
                if(level==number_of_levels-1){
                    ptw->stats.number_of_2MB_pages++; 
                }
                e->phy_addr = phy_addr;
                phy_addr++;
            }
            else{
                e->entry_type=ptw_table_entry_type::PTW_TABLE_POINTER;
                e->next_level_table=InitiateTablePtw((int)pow(2.0,(float)level_bit_indices[level]), verify_metadata);
            }
        } else{
            //last level
            e->entry_type=ptw_table_entry_type::PTW_ADDRESS;
            e->phy_addr = phy_addr;
            phy_addr++;
        }
        // Merkle-tree verification
        if (verify_metadata->verify_typ==VERIFY_MT){
            (e->mt_pte) = mt_create();
            UInt64* data = (UInt64 *) malloc(sizeof(UInt64)); // assume 4KB page table size at the worst case
            std::cout << "the level of the entry in PTW is : " << level << std::endl;
            // Done TODO: Change to compare with ptw_table_entry_type instead of with levels
            
            // @hong: create a new MT only if the verification is done in page table entry level
            if (verify_metadata->mt_verify_gran == PTE_LEVEL){
                if (e->entry_type!=ptw_table_entry_type::PTW_ADDRESS){
                    printf("%d", sizeof(struct ptw_table*));
                    memcpy(&data, e->next_level_table, sizeof(UInt64));
                    data[0] = (uint64_t) e->next_level_table;  // address to the next page table
                } else {
                    data[0] = e->phy_addr; // physical address
                }
                InitiateMT(e->mt_pte, data, verify_metadata);
            } 

            // e->phy_addr =
            // printf("initiate ptw entry\n"); 
            // mt_print(e->mt);
            // if (e->entry_type!=ptw_table_entry_type::PTW_ADDRESS){
            //     std::cout << "Data is entry table and is : " << data << std::endl;       
            // } else {
            //     std::cout << "Data is physical address and is : " << e->phy_addr << std::endl;       
            // 
            
        }

        return e;
    }

    /** 
     * Initiate the page table
     * 
     * 
     * 
    */
    ptw_table* InitiateTablePtw(int size, verify_struct* verify_metadata){

        printf("InitiateTablePtw called\n");

        // size = 512
        ptw_table* t=(ptw_table*)malloc(sizeof(ptw_table));
        t->table_size= size; //verify_metadata->table_size;
        
        t->mt_pt = mt_create();
    
        // initiate page table entries => 4KB
        t->entries=(ptw_table_entry*) malloc(verify_metadata->table_size*sizeof(ptw_table_entry)); // 4KB => 512 x 64bit 
        for (int i = 0; i < verify_metadata->table_size; i++)
        {
            t->entries[i].entry_type=ptw_table_entry_type::PTW_NONE;
        }
        UInt64 * data = (uint64_t*) malloc(verify_metadata->total_entry*sizeof(UInt64)); // 512*sizeof(UInt64)

        if(verify_metadata->mt_verify_gran == PT_LEVEL){
            for (int i = 0; i < verify_metadata->total_entry; i++){
                data[i] = (UInt64)(t->entries+i);
            }
            InitiateMT(t->mt_pt, data, verify_metadata);
        }
        
        if (verify_metadata->mt_verify_gran == CL_LEVEL){
            for (int j = 0; j < verify_metadata->n_cl_blocks; j++){
                for (int i = 0; i < verify_metadata->total_entry; i+=verify_metadata->cl_size){
                    data[i] = (UInt64)(t->entries+j*verify_metadata->total_entry+i);
                }
                t->mt_cl[j] = mt_create();

                InitiateMT(t->mt_cl[j], data, verify_metadata);
            }
        }
        printf("InitiateTablePtw done\n");
        return t;
    }

    /**
     * Extract [begin, end) bits of a 64 bits value (we assume bit index starts at 0)
     * 
     */ 
    uint64_t extract(uint64_t value, uint16_t begin, uint16_t end){
        // std::cout << "Begin" << begin << "End" << end << std::endl;
        uint64_t mask =(uint64_t)(((uint64_t)1 << ((uint64_t)end - (uint64_t )begin))) - 1;
        // std::cout << "Mask: " << mask << "and value: " << value << std::endl;
        return (value >> begin) & mask;
    }

    /** 
     * Hash data into hash_values
     * 
     * data (uint64_t *):
     * hash_values (uint8_t **):
     * verify_metadata (verify_struct*):
     * 
     */
    void HashValue(UInt64* data, uint8_t **hash_values, verify_struct* verify_metadata){
        // printf("HashValue called\n");
        // divide each hash_values[i] into smaller sub-partition of 8-bits (1-byte), where 1 hash_value contains 32 of 8 bits (256 bits in total)
        // uint16_t n_subpartition; // the sub-partition of the partition

        uint8_t* data_values = (uint8_t*) malloc(HASH_LENGTH*sizeof(uint8_t));  // 32 * 8
        uint64_t* data_partition = (uint64_t*) malloc(4*sizeof(uint64_t)); // 256 bits
        for (int i  = 0; i< 4; i++){
            data_partition[i] = 0llu;
        }
        // data_partition = {}; // make data_partition 0
        mt_error_t error;
        mt_hash_t data_values_hash; 
        printf("1\n");

        if (verify_metadata->mt_verify_gran == PTE_LEVEL){
            for (int j = 0; j < verify_metadata->n_partition; j++){
                // if (verify_metadata->n_partition == 1){
                //     data_partition[0] = data[0];
                // } else{
                // data => data_partition => data_values => data_values_hash => hash_values
                data_partition[0] = extract(data[j], 0,verify_metadata->mt_node_size); 
                DataToByte(data_partition, data_values, verify_metadata->n_subpartition, verify_metadata->mt_node_size); 
                mt_init_hash(data_values_hash, data_values, verify_metadata->n_subpartition);
                error = mt_hash_val(data_values_hash, hash_values[j]);
                data[j] >>= verify_metadata->mt_node_size;
            }
        } else {
            int i = 0;
            int data_idx = 0;
            // printf("2\n");
            while (i < verify_metadata->n_partition){
                // we need to divide 64-bit entries to smaller chunks
                // printf("node %d\n",i);
                // printf("node size: %u\n", verify_metadata->mt_node_size);
                // printf("addr_len: %u \n", verify_metadata->addr_len);
                // printf("n_data_partition: %u\n", verify_metadata->n_data_partition);
                printf("data: %llu \n", data[data_idx]);
                // printf("total_size: %llu\n", verify_metadata->total_size);
                if(verify_metadata->mt_node_size < verify_metadata->addr_len){
                    for (int j = 0; j < verify_metadata->n_data_partition; j++){
                        // printf("Offset: %d, Bit_index: %d, data_idx: %d, data[data_idx]: %llu \n", i, j, data_idx, data[data_idx]);
                        data_partition[0] = extract(data[data_idx], 0, verify_metadata->mt_node_size);
                        // printf("data partition: %llu\n", data_partition[0]);
                        DataToByte(data_partition, data_values, verify_metadata->n_subpartition, verify_metadata->mt_node_size); 
                        // PrintHash(data_values);
                        mt_init_hash(data_values_hash, data_values, verify_metadata->n_subpartition);
                        error = mt_hash_val(data_values_hash, hash_values[i]);
                        data[data_idx] >>= verify_metadata->mt_node_size; 
                    }
                    i+=verify_metadata->n_data_partition;
                    data_idx++;
                } else {
                    for (int j = 0 ; i*verify_metadata->mt_node_size + j < 256; j+=verify_metadata->addr_len){
                        data_partition[j] = data[i*verify_metadata->mt_node_size + j];
                    }
                    DataToByte(data_partition, data_values, verify_metadata->n_subpartition, verify_metadata->mt_node_size); 
                    mt_init_hash(data_values_hash, data_values, verify_metadata->n_subpartition);
                    error = mt_hash_val(data_values_hash, hash_values[i]);
                    i++;
                }
            }

        }
        // printf("HashValue ended\n");

        free(data_values);
    }

    /**
     * 
     * data (uint64_t *): an array of 3 types 1) Page table entry (1 x 64) => 1 element
     *                                        2) Page table (512 x 64 bits) => 512 elements
     *                                        3) Cache line (8 x 64 bits) => 8 elements
     * 
     */
    mt_error_t InitiateMT(mt_t* mt, UInt64* data, verify_struct* verify_metadata){
        printf("InitiateMT called\n");
        // hash_
        uint8_t **hash_values;

        // std::cout << "Number of partitions: " << n_partition << std::endl;
        hash_values = (uint8_t **) malloc(sizeof(uint8_t*)*verify_metadata->n_partition);
        for(uint i=0; i < verify_metadata->n_partition; i++){
            hash_values[i] = (uint8_t*) malloc(sizeof(uint8_t)*HASH_LENGTH);
        }

        HashValue(data, hash_values, verify_metadata);

        // PrintHash(hash_values);
        mt_error_t error;
        // add each node into the merkle tree
        printf("n_partition: %llu \n", verify_metadata->n_partition);
        
        for (uint i = 0; i < verify_metadata->n_partition; i++){
            // printf("verify_typ: %d\n", verify_metadata->verify_typ);
            mt_print_hash(hash_values[i]);
            mt_print(mt);

            error = mt_add(mt, hash_values[i], HASH_LENGTH);
            // printf("%d", (int)error);
            if (error != MT_SUCCESS){
                printf("MT error %d occurred!\n", (int) error);
            }
        }
        // mt_print(mt);
        // std::cout << mt->elems << std::endl;
        printf("InitiateMT finished\n");

        return MT_SUCCESS;
    }

    /** Convert data in a pointer of 64 bits into 32 of 8 bits 
     * data_partition (UInt64 *) : a page table entry (256 bits maximum)
     * tags (uint8_t *) : a tag  (32 x 8 bits)
     * 
     *     
    */ 
    void DataToByte(UInt64* data_partition, uint8_t* tags, uint16_t n_subpartition, uint16_t mt_node_size){
        uint8_t data_subpartition;
        // int step_size = (mt_node_size < 8)? mt_node_size : 8;
        // int data_idx = 0;
        // for (int j =0; j < 4; j++){
        //     for (int i = 0; i <  n_subpartition; i++){
        //         data_subpartition = extract(data_partition[j], 0, 8);// data % (1 << 8);
        //         tags[i] = data_subpartition;
        //         data_partition[j] >>= 8; 
        //     }
        // }
        printf("DataToByte called\n");
        for (int i = 0; i < 4; i++){
            // printf("data_partition[%d]: %llu\n", i, data_partition[i]);
            for (int j = 0; j < 8; j++){
                data_subpartition = extract(data_partition[i], 0, 8);
                tags[i*8+j] = data_subpartition;
                // printf("tag[%d]: %llu\n", i*8+j, data_subpartition);
                data_partition[i]>>=8;
            }
        }
        printf("DataToByte finished\n");

    }

    /**
        size: the size of each node (16 bits)
        addrs: addresses to be visited
        level: the number of levels (the number of addresses) visited
    */
    bool verify_mt(mt_t *mt, UInt64* data, verify_struct* verify_metadata, uint64_t *addrs, uint8_t *level, uint32_t offset){
        mt_error_t error;
        uint64_t data_partition[4];
        uint8_t *data_values;
        data_values = (uint8_t *) malloc(sizeof(uint8_t)*HASH_LENGTH);
        // hash_value = (uint8_t *) malloc(sizeof(uint8_t)*HASH_LENGTH); // hashed data
        // uint8_t data_node[n_subpartition];
        // std::cout << "verify_mt is called" << std::endl;

        // printf("split: %u \n", splits);
        if (!offset)
            offset = std::rand() % (verify_metadata->n_partition); // find a random node to be verified
        uint32_t data_idx, bit_idx;
        if (verify_metadata->mt_node_size < verify_metadata->addr_len){
            // offset = bit_idx + addr_len/mt_node_size * data_idx 
            bit_idx = offset % (verify_metadata->addr_len/verify_metadata->mt_node_size);
            data_idx = (offset - bit_idx) * verify_metadata->mt_node_size/verify_metadata->addr_len;
            data_partition[0] = extract(data[data_idx], bit_idx*verify_metadata->mt_node_size, (bit_idx+1)*verify_metadata->mt_node_size);

        } else{
            for (int i = 0; i < verify_metadata->n_data_partition; i++){
                data_partition[i] = extract(data[i], 0, verify_metadata->addr_len); //[0:63]
            }
        }
        // std::cout << "Offset: " << offset << std::endl;
        // std::cout << "offset size begin: "<< offset*size << std::endl;
        // std::cout << "offset size end: " << (offset+1)*size << std::endl;
        DataToByte(data_partition, data_values, verify_metadata->n_subpartition, verify_metadata->mt_node_size);

        // std::cout << "Verification data is: " << data << std::endl;
        // for (int i = 0; i < HASH_LENGTH; i++){
        //     std::cout << hash_value[0][i] << std::endl;
        // }

        // printf("data for verify_mt: %llu\n", data);
        // mt_print_hash(hash_value[0]);
        
        // mt_print(mt);
        error = mt_verify(mt, data_values, HASH_LENGTH, offset, addrs, level);

        return error==MT_SUCCESS;
    } 

    SubsecondTime GetMTLatency(mt_t *mt, int cycles){
        // 1. mt creation
        // we need to perform hashing the same number of nodes in each level
        SubsecondTime period = SubsecondTime::SEC() / (2.6*1000000000);
        uint32_t nodes = mt->elems; // the number of nodes in the merkle tree
        uint32_t count = 0;
        uint32_t levels = ((uint32_t) log2(nodes))+1;
        SubsecondTime hash = cycles*period;

        for (uint32_t i = 0; i < levels; i++){
            count += (uint32_t)pow(2, i);
        }
        return hash * count;

    }

    SubsecondTime GetVerifyLatency(mt_t *mt, int cycles){
        // 2. verification
        // we need to perform one hashing over each level except at the root

        SubsecondTime period = SubsecondTime::SEC() / (2.6*1000000000);
        uint32_t nodes = mt->elems; // the number of nodes in the merkle tree
        uint32_t levels = ((uint32_t) log2(nodes))+1;
        uint32_t count = 0;
        SubsecondTime hash = cycles*period;

        for (uint32_t i = 1; i < levels; i++){
            count += 1;
        }
        
        return hash * count;
    }
    
    void PrintHash(uint8_t* tag){
        printf("HashValue: ");
        for (int i = HASH_LENGTH-1; i>=0; i--){
            printf("%d", tag[i]);
        }
        // for (int i = 0; i<HASH_LENGTH; i++){
        //     printf("%d", tag[i]);
        // }
        printf("\n");
    }



    // void HashValue(UInt64 data, uint8_t **hash_values, uint16_t n_partition, uint16_t mt_node_size){

    //     // size = size of data for each node => 16 bits for us
    //     uint n_subpartition;
    //     if (mt_node_size < 8){ // the number of bits per node less than 8 (1, 2, 4 bits)
    //         n_subpartition = 1;
    //     } else{
    //         n_subpartition = mt_node_size / 8; // we have 32 of 8 bits blocks to store 256-bits of hashing
    //     }
    //     // printf("n_subpartition: %d\n", n_subpartition);
    //                                         // this value indicates how many of blocks are nonzeros
    //     uint8_t* data_values = (uint8_t*) malloc(HASH_LENGTH*sizeof(uint8_t));  // 32 * 8

    //     UInt64 data_partition;

    //     UInt64 data_subpartition;
    //     // int counter = 0;
    //     mt_error_t error;
    //     mt_hash_t data_values_hash; 
    //     // std::cout << "Sub partition: " << n_subpartition << std::endl;
    //     // std::cout << "HashValue called for data: " << data << std::endl;
    //     for (uint i = 0; i < n_partition; i++){
    //         // std::cout << "Data: " << data << std::endl;
    //         if (n_partition == 1){
    //             data_partition = data;
    //         } else{
    //             data_partition = extract(data, 0, mt_node_size); // take 16 bits
    //         }
    //         // std::cout << "Data partition: " << data_partition << " with size: " << size << std::endl;
    //         // distribute the data_partition = the node 
    //         // into a block of 1-byte sequences 
    //         // e.g.) 16-bits for 1 leave, we need 2 blocks of 8 bits
    //         DataToByte(data_partition, data_values, n_subpartition); 
    //         // for (uint j = 0; j < n_subpartition; j++){
    //         //     data_subpartition = extract(data_partition, 0 , 8);
    //         //     // std::cout << "Data subpartition: "<< data_subpartition << std::endl;
    //         //     data_values[j] = data_subpartition;
    //         //     data_partition >>= 8;
    //         //     // counter++;
    //         // }
    //         mt_init_hash(data_values_hash, data_values, n_subpartition);
    //         error = mt_hash_val(data_values_hash, hash_values[i]);
    //         // for (int j = 0; j < HASH_LENGTH; j++){
    //         //     std::cout << hash_values[i][j] << std::endl;
    //         // }
    //         // if (error != MT_SUCCESS){
    //         //     printf("Hashing has failed!\n");
    //         // }
    //         data >>= mt_node_size;
    //     }
    //     // 64-bits
    //     // 16-bit 4x => hash 256 4x 
    //     // merkle over 4 values of hash 256

    //     // 64-bits => hash 256 1x
    //     // 
    //     // => 256/4 => hash over these MT
    //     free(data_values);
    // }
}

