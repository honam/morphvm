
#include "cache_cntlr.h"
#include "pwc.h"
#include "subsecond_time.h"
#include "memory_manager.h"
#include "pagetable_walker.h"
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <math.h>
#include <fstream>
#include "hashtable_baseline.h"
namespace ParametricDramDirectoryMSI{
    HashTablePTW::HashTablePTW(int _table_size_in_bits,int _small_page_size_in_bits,int _large_bage_size_in_bits,int _small_page_percentage,
        Core* _core,ShmemPerfModel* _m_shmem_perf_model,PWC* pwc, bool pwc_enabled)
    :PageTableWalker(_core->getId(), 0, _m_shmem_perf_model, pwc, pwc_enabled){
        this->table_size_in_bits=_table_size_in_bits;
        this->small_page_size_in_bits=_small_page_size_in_bits;
        this->large_page_size_in_bits=_large_bage_size_in_bits;
        this->small_page_percentage=_small_page_percentage;
        this->core=_core;
        this->m_shmem_perf_model=_m_shmem_perf_model;
        
        this->small_page_table=(entry *)malloc((UInt64)pow(2,table_size_in_bits)*sizeof(entry));
        for (UInt64 i = 0; i < (UInt64)pow(2,table_size_in_bits); i++)
        {
            this->small_page_table[i].empty=true;
            this->small_page_table[i].vpn=0;
            this->small_page_table[i].next_entry=NULL;
        }

        this->large_page_table=(entry *)malloc((UInt64)pow(2,table_size_in_bits)*sizeof(entry));
        for (UInt64 i = 0; i < (UInt64)pow(2,table_size_in_bits); i++)
        {
            this->large_page_table[i].empty=true;
            this->large_page_table[i].vpn=0;
            this->large_page_table[i].next_entry=NULL;
        }
    
    
    }
    int HashTablePTW::hash_function(IntPtr address){
        uint64 mask=0;
        for(int i=0 ;i<table_size_in_bits;i++){
            mask+=(int)pow(2,i);
        }
        char*  new_address = (char*) address;
        uint64 result=CityHash64((const char*)&address,8) & (mask);
        //std::cout<<std::hex<<address<<" "<<std::hex<<result<<" "<<std::hex<<mask<<"\n";
        return result;
    }
    SubsecondTime HashTablePTW::init_walk(IntPtr address, UtopiaCache* shadow_cache,
    CacheCntlr *cache, Core::lock_signal_t lock_signal, Byte* _data_buf, 
    UInt32 _data_length, bool modeled, bool count){
        SubsecondTime delay=SubsecondTime::Zero();
        SubsecondTime start=getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
        int hash_function_result=hash_function(address);
        entry *current_entry_small=&small_page_table[hash_function_result];
        entry *current_entry_large=&large_page_table[hash_function_result];
        //Code I am bored to make a function
        SubsecondTime t_start = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);                
        IntPtr cache_address = ((IntPtr)(current_entry_small)) & (~((64 - 1))); 
            cache->processMemOpFromCore(
                lock_signal,
                Core::mem_op_t::READ,
                cache_address, 0,
                data_buf, data_length,
                modeled,
                count, CacheBlockInfo::block_type_t::PAGE_TABLE, SubsecondTime::Zero());
        SubsecondTime t_end = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
        if(nuca){
            nuca->markTranslationMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
            mem_manager->getCache(MemComponent::component_t::L1_DCACHE)->markMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
            mem_manager->getCache(MemComponent::component_t::L2_CACHE)->markMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
        }
        getShmemPerfModel()->setElapsedTime(ShmemPerfModel::_USER_THREAD,t_start);
        SubsecondTime t_start_1 = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);                
        cache_address = ((IntPtr)(current_entry_large)) & (~((64 - 1))); 
            cache->processMemOpFromCore(
                lock_signal,
                Core::mem_op_t::READ,
                cache_address, 0,
                data_buf, data_length,
                modeled,
                count, CacheBlockInfo::block_type_t::PAGE_TABLE, SubsecondTime::Zero());
        SubsecondTime t_end_1 = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
        delay += std::max(t_end - t_start ,t_end_1-t_start_1);
        getShmemPerfModel()->setElapsedTime(ShmemPerfModel::_USER_THREAD,t_start+std::max(t_end - t_start ,t_end_1-t_start_1));
        //Code I am bored to make a function
        if(nuca){
            nuca->markTranslationMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
            mem_manager->getCache(MemComponent::component_t::L1_DCACHE)->markMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
            mem_manager->getCache(MemComponent::component_t::L2_CACHE)->markMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
        }
        
        
        while(!current_entry_small->empty || !current_entry_large->empty){
            if(current_entry_small->vpn==(address>>(small_page_size_in_bits+2))){
                //Code I am bored to make a function
                t_start = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);                
                cache_address =((IntPtr)(&current_entry_small->data[address%(int)pow(2,small_page_size_in_bits)])) & (~((64 - 1))); 
                    cache->processMemOpFromCore(
                        lock_signal,
                        Core::mem_op_t::READ,
                        cache_address, 0,
                        data_buf, data_length,
                        modeled,
                        count, CacheBlockInfo::block_type_t::PAGE_TABLE, SubsecondTime::Zero());
                t_end = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
                delay += t_end - t_start ;
                getShmemPerfModel()->setElapsedTime(ShmemPerfModel::_USER_THREAD,start);
                if(nuca){
                    nuca->markTranslationMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
                    mem_manager->getCache(MemComponent::component_t::L1_DCACHE)->markMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
                    mem_manager->getCache(MemComponent::component_t::L2_CACHE)->markMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
                }
                UInt64 vpn = address >> init_walk_functional(address);
                track_per_page_ptw_latency(vpn,delay);
                return delay;
            }
            else if(current_entry_large->vpn==(address>>(large_page_size_in_bits+2))){
                //Code I am bored to make a function
                t_start = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);                
                cache_address =((IntPtr)(&current_entry_large->data[address%(int)pow(2,large_page_size_in_bits)])) & (~((64 - 1))); 
                    cache->processMemOpFromCore(
                        lock_signal,
                        Core::mem_op_t::READ,
                        cache_address, 0,
                        data_buf, data_length,
                        modeled,
                        count, CacheBlockInfo::block_type_t::PAGE_TABLE, SubsecondTime::Zero());
                t_end = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
                delay += t_end - t_start ;
                getShmemPerfModel()->setElapsedTime(ShmemPerfModel::_USER_THREAD,start);
                if(nuca){
                    nuca->markTranslationMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
                    mem_manager->getCache(MemComponent::component_t::L1_DCACHE)->markMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
                    mem_manager->getCache(MemComponent::component_t::L2_CACHE)->markMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
                }
                UInt64 vpn = address >> init_walk_functional(address);
                track_per_page_ptw_latency(vpn,delay);
                return delay;
            }
            else{
                if(current_entry_small->next_entry!=NULL){
                    t_start = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);                
                    cache_address = ((IntPtr)(current_entry_small)) & (~((64 - 1))); 
                        cache->processMemOpFromCore(
                            lock_signal,
                            Core::mem_op_t::READ,
                            cache_address, 0,
                            data_buf, data_length,
                            modeled,
                            count, CacheBlockInfo::block_type_t::PAGE_TABLE, SubsecondTime::Zero());
                    t_end = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
                    if(nuca){
                        nuca->markTranslationMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
                        mem_manager->getCache(MemComponent::component_t::L1_DCACHE)->markMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
                        mem_manager->getCache(MemComponent::component_t::L2_CACHE)->markMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
                    }
                    if(current_entry_large->next_entry!=NULL){
                        getShmemPerfModel()->setElapsedTime(ShmemPerfModel::_USER_THREAD,t_start);
                        t_start_1 = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);                
                        cache_address = ((IntPtr)(current_entry_large)) & (~((64 - 1))); 
                            cache->processMemOpFromCore(
                                lock_signal,
                                Core::mem_op_t::READ,
                                cache_address, 0,
                                data_buf, data_length,
                                modeled,
                                count, CacheBlockInfo::block_type_t::PAGE_TABLE, SubsecondTime::Zero());
                        t_end_1 = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
                        delay += std::max(t_end - t_start ,t_end_1-t_start_1);
                        getShmemPerfModel()->setElapsedTime(ShmemPerfModel::_USER_THREAD,t_start+std::max(t_end - t_start ,t_end_1-t_start_1));
                        current_entry_large=current_entry_large->next_entry;
                        if(nuca){
                            nuca->markTranslationMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
                            mem_manager->getCache(MemComponent::component_t::L1_DCACHE)->markMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
                            mem_manager->getCache(MemComponent::component_t::L2_CACHE)->markMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
                        }
                    }
                    else{
                        delay+=t_end-t_start;
                    }
                    current_entry_small=current_entry_small->next_entry;
                }
                else{
                    if(current_entry_large->next_entry!=NULL){
                        getShmemPerfModel()->setElapsedTime(ShmemPerfModel::_USER_THREAD,t_start);
                        t_start_1 = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);                
                        cache_address = ((IntPtr)(current_entry_large)) & (~((64 - 1))); 
                            cache->processMemOpFromCore(
                                lock_signal,
                                Core::mem_op_t::READ,
                                cache_address, 0,
                                data_buf, data_length,
                                modeled,
                                count, CacheBlockInfo::block_type_t::PAGE_TABLE, SubsecondTime::Zero());
                        t_end_1 = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
                        delay += t_end_1-t_start_1;
                        if(nuca){
                            nuca->markTranslationMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
                            mem_manager->getCache(MemComponent::component_t::L1_DCACHE)->markMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
                            mem_manager->getCache(MemComponent::component_t::L2_CACHE)->markMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
                        }
                    }
                }
            }
        }
        int random_number=rand()%100;
        if(random_number<small_page_percentage){
            if(current_entry_small->empty==false){
                current_entry_small->empty=false;
                current_entry_small->vpn=address >> (small_page_size_in_bits+2);
                current_entry_small->data=(int *)malloc((int)pow(2,small_page_size_in_bits)*sizeof(int));
                current_entry_small->next_entry=NULL;
            }
            else{
                current_entry_small->next_entry=(entry *)malloc(sizeof(entry));
                current_entry_small->next_entry->empty=true;
            }
            
            t_start = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);                
            cache_address = ((IntPtr)(&current_entry_small->data[address%(int)pow(2,small_page_size_in_bits)])) & (~((64 - 1))); 
                cache->processMemOpFromCore(
                    lock_signal,
                    Core::mem_op_t::READ,
                    cache_address, 0,
                    data_buf, data_length,
                    modeled,
                    count, CacheBlockInfo::block_type_t::PAGE_TABLE, SubsecondTime::Zero());
            t_end = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
            delay += t_end - t_start ;
            if(nuca){
                nuca->markTranslationMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
                mem_manager->getCache(MemComponent::component_t::L1_DCACHE)->markMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
                mem_manager->getCache(MemComponent::component_t::L2_CACHE)->markMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
            }
        }
        else{
            stats.number_of_2MB_pages++;
            if(current_entry_large->empty==false){
                current_entry_large->empty=false;
                current_entry_large->vpn=address >> (large_page_size_in_bits+2);
                current_entry_large->data=(int *)malloc((int)pow(2,large_page_size_in_bits)*sizeof(int));
                current_entry_large->next_entry=NULL;
            }
            else{
                current_entry_large->next_entry=(entry *)malloc(sizeof(entry));
                current_entry_large->next_entry->empty=true;
            }
            
            t_start = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);                
            cache_address = ((IntPtr)(&current_entry_large->data[address%(int)pow(2,large_page_size_in_bits)])) & (~((64 - 1))); 
                cache->processMemOpFromCore(
                    lock_signal,
                    Core::mem_op_t::READ,
                    cache_address, 0,
                    data_buf, data_length,
                    modeled,
                    count, CacheBlockInfo::block_type_t::PAGE_TABLE, SubsecondTime::Zero());
            t_end = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
            delay += t_end - t_start ;
            if(nuca){
                nuca->markTranslationMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
                mem_manager->getCache(MemComponent::component_t::L1_DCACHE)->markMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
                mem_manager->getCache(MemComponent::component_t::L2_CACHE)->markMetadata(cache_address,CacheBlockInfo::block_type_t::PAGE_TABLE);
            }
        }
        
        //Code I am bored to make a function
        getShmemPerfModel()->setElapsedTime(ShmemPerfModel::_USER_THREAD,start);
        UInt64 vpn = address >> init_walk_functional(address);
        track_per_page_ptw_latency(vpn,delay);
        return delay;
        
        
    }
    bool HashTablePTW::isPageFault(IntPtr address){

        SubsecondTime delay=SubsecondTime::Zero();
        SubsecondTime start=getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
        int hash_function_result=hash_function(address);
        entry *current_entry_small=&small_page_table[hash_function_result];
        entry *current_entry_large=&large_page_table[hash_function_result];
        while(!current_entry_small->empty || !current_entry_large->empty){
            if(current_entry_small->vpn==(address>>(small_page_size_in_bits+2))){
                return false;
            }
            else if(current_entry_large->vpn==(address>>(large_page_size_in_bits+2))){
                return false;
            }
            else{
                if(current_entry_small->next_entry!=NULL){
                    if(current_entry_large->next_entry!=NULL){
                        current_entry_large=current_entry_large->next_entry;
                    }
                    current_entry_small=current_entry_small->next_entry;
                }
                else{
                    if(current_entry_large->next_entry!=NULL){
                        current_entry_large=current_entry_large->next_entry;
                    }
                }
                
                    
            }
        }
        return true;
    }
        
    

    int HashTablePTW::init_walk_functional(IntPtr address){
        SubsecondTime delay=SubsecondTime::Zero();
        SubsecondTime start=getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
        int hash_function_result=hash_function(address);
        entry *current_entry_small=&small_page_table[hash_function_result];
        entry *current_entry_large=&large_page_table[hash_function_result];
        while(!current_entry_small->empty || !current_entry_large->empty){
            if(current_entry_small->vpn==(address>>(small_page_size_in_bits+2))){
                return small_page_size_in_bits;
            }
            else if(current_entry_large->vpn==(address>>(large_page_size_in_bits+2))){
                return large_page_size_in_bits;
            }
            else{
                if(current_entry_small->next_entry!=NULL){
                    if(current_entry_large->next_entry!=NULL){
                        current_entry_large=current_entry_large->next_entry;
                    }
                    current_entry_small=current_entry_small->next_entry;
                }
                else{
                    if(current_entry_large->next_entry!=NULL){
                        current_entry_large=current_entry_large->next_entry;
                    }
                }
                
                    
            }
        }
        return small_page_size_in_bits;
    }
}