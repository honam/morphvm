#ifndef PTWV_H
#define PTWV_H

#include "pagetable_walker.h"
#include "fixed_types.h"
#include "cache.h"
#include <unordered_map>
#include "cache_cntlr.h"
#include <random>
#include "pwc.h"
#include "utopia_cache_template.h"

namespace ParametricDramDirectoryMSI
{
   class PageTableWalkerVirtual: public PageTableWalker{

      private:

	    IntPtr gCR3; //virtualized only
       bool utopia_enabled;

      UInt64 virtual_page_walks;
		UInt64 utopia_hits;
		UInt64 utopia_misses;
		SubsecondTime latency_virtual;
      PageTableWalker *ptw1;
      PageTableWalker *ptw2;


	   public:

		  PageTableWalkerVirtual(int core_id, int pagesize, ShmemPerfModel* m_shmem_perf_model,PWC* pwc, bool pwc_enabled);
        void step_walk_virtual(IntPtr address, IntPtr pointer_PMLE4, IntPtr pointer_PDPE, IntPtr pointer_PD, IntPtr pointer_PT, int level, bool virtual_walk, bool nested);
        SubsecondTime init_walk(IntPtr address,UtopiaCache* shadow_cache, CacheCntlr *cache, Core::lock_signal_t lock_signal, Byte* _data_buf, UInt32 _data_length, bool modeled, bool count) override;
        int init_walk_functional(IntPtr address){return 0;}
        bool isPageFault(IntPtr address){return false;}
   
   };

}

#endif
 
