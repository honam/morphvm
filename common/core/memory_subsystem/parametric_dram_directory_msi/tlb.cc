#include "tlb.h"
#include "stats.h"
#include "config.hpp"
#include <cmath>
#include <iostream>
#include <utility>
#include "core_manager.h"
#include "cache_set.h"
#include "cache_base.h"
#include "utils.h"
#include "log.h"
#include "rng.h"
#include "address_home_lookup.h"
#include "fault_injection.h"
//#define DEBUG_TLB
#define TLB_STATS

namespace ParametricDramDirectoryMSI
{

  UInt32 TLB::SIM_PAGE_SHIFT;
  IntPtr TLB::SIM_PAGE_SIZE;
  IntPtr TLB::SIM_PAGE_MASK;

         
  TLB::TLB(String name, String cfgname, core_id_t core_id, UInt32 num_entries,UInt32 pagesize, UInt32 associativity, TLB *next_level, bool _utopia_enabled, bool _track_misses, bool _track_accesses, int* page_size_list, int page_sizes, PageTableWalker*  _ptw)
    : m_size(num_entries)
    , m_core_id(core_id)
    , m_associativity(associativity)
    , m_cache(name + "_cache", 
            cfgname, 
            core_id, num_entries / associativity, 
            associativity, (1L << pagesize), "lru", 
            CacheBase::PR_L1_CACHE, CacheBase::HASH_MASK,
            NULL,
            NULL, true, page_size_list, page_sizes)
    , m_next_level(next_level)
    , m_access(0)
    , m_miss(0)
    , m_tlb_address_saved_cnt(0)
    , utopia_enabled(_utopia_enabled)
    , track_L2TLBmiss(_track_misses)
    , track_Accesses(_track_accesses)
  {
    LOG_ASSERT_ERROR((num_entries / associativity) * associativity == num_entries, "Invalid TLB configuration: num_entries(%d) must be a multiple of the associativity(%d)", num_entries, associativity);

    registerStatsMetric(name, core_id, "access", &m_access);
    registerStatsMetric(name, core_id, "miss", &m_miss);

  
    
    m_num_entries = num_entries;

    is_dtlb = (name == "dtlb");
    ptw = _ptw;


    /* @kanellok UTOPIA related parameters */
    if(utopia_enabled){
      m_utopia =  Sim()->getUtopia();
      m_utr_4KB = m_utopia->getUtr(0);
      m_utr_2MB = m_utopia->getUtr(1);
    }
    if(is_dtlb && m_next_level){
      
          registerStatsMetric(name, core_id, "L2_TLB_miss_per_page", &tlb_address_access, true); // @kanellok @tlb_address_access 
                                                                                                 // does not matter here as we process the metrics at the end
    }
    
  }

  TLB::where_t TLB::lookup(IntPtr address, SubsecondTime now, bool allocate_on_miss, int level, bool model_count)
  {
    //TODO model bitmap cache access here

    if(ptw->isPageFault(address) && utopia_enabled && m_utopia->getHeurPrimary() ==  Utopia::utopia_heuristic::pf ){
      std::cout<< "Page fault address is: "<< address << std::endl;
      int page_size = ptw->init_walk_functional(address);
      std::cout << "Page size = "<< page_size << std::endl;
      if(page_size == 12)
        m_utr_4KB->allocate(address, now, m_core_id);
      else if(page_size == 21)
        m_utr_2MB->allocate(address, now, m_core_id);

    }

    m_access++;

    int page_size = ptw->init_walk_functional(address);
    IntPtr vpn = address >> page_size;

    #ifdef DEBUG_TLB
      //std::cout << " Lookup: " << vpn << "at level: " << level << " Page size: " << page_size <<  std::endl;
    #endif


    #ifdef TLB_STATS
      if(model_count && track_Accesses){
        if(access_per_page.find(vpn) != access_per_page.end() && (level == 1)){
            access_per_page[vpn] += 1;
        }
          else{
            access_per_page[vpn] = 1; //@kanellok Track accesses per page
          }
        
            
      }
    #endif


    if(utopia_enabled && is_dtlb)
    {

        bool skip = false;

        if (m_utr_4KB->inUTR(address,model_count,now,m_core_id)) skip = true; //@kanellok check if data is in UTR and just skip the TLB access in such case
        if (m_utr_2MB->inUTR(address,model_count,now,m_core_id)) skip = true; //@kanellok check if data is in UTR and just skip the TLB access in such case

        if(skip) return where_t::UTR_HIT;
        
    }

    bool hit = m_cache.accessSingleLineTLB(address, Cache::LOAD, NULL, 0, now, true);

    #ifdef DEBUG_TLB

    //  if(hit)   std::cout << " Hit at level: " << level  <<  std::endl;
      if(!hit)      std::cout << " Miss at level: " << level  <<  std::endl;

    #endif

    if (hit) return level == 1 ? where_t::L1 : where_t::L2;

    if(model_count) m_miss++; // We reach this point if L1 TLB Miss

    TLB::where_t where_next = TLB::MISS;
   
    bool l2tlb_miss = true;
    
    if (m_next_level) // is there a second level TLB?
    {
        where_next = m_next_level->lookup(address, now, false, 2 /* no allocation */,model_count); 
        if( where_next != TLB::MISS)
          l2tlb_miss = false;
    }


    bool allocated_in_utopia = false;

    if(l2tlb_miss && utopia_enabled && level == 1){

        if((m_utopia->getHeurSecondary() == Utopia::utopia_heuristic::pte) ){

            if(pte_pressure_cnt.find(vpn) != pte_pressure_cnt.end() ){ // @kanellok Track the pressure at each PTE entry
                    pte_pressure_cnt[vpn] += 1;
            }
            else {
                    pte_pressure_cnt[vpn] = 1;
            }


            if(pte_pressure_cnt[vpn] > m_utopia->getPTEthr()){ //@kanellok if PTE entry is "hot", allocate the page in UTR and dont insert in the TLB
                  
                  allocated_in_utopia = true;
                  pte_pressure_cnt[vpn] = 0 ;

                  if(page_size == 12)
                    m_utr_4KB->allocate(address,now, m_core_id);
                  if(page_size == 21)
                    m_utr_2MB->allocate(address,now, m_core_id);

                  where_next = TLB::L1;
            }
        
        
        }

    }

    if (allocate_on_miss && !allocated_in_utopia ) //@kanellok allocate in L1TLB
    {
        allocate(address, now, level);
    }

    #ifdef TLB_STATS
    
      if(where_next == TLB::MISS && level == 1 && model_count && track_L2TLBmiss){ // Track L2 TLB misses per page

        if(L2TLB_miss_per_page.find(vpn) != L2TLB_miss_per_page.end()) 
            L2TLB_miss_per_page[vpn] += 1;
        else 
            L2TLB_miss_per_page[vpn] = 1; 

      }
    #endif

     return where_next;
  }

  void TLB::allocate(IntPtr address, SubsecondTime now, int level)
  {

    

    int page_size = ptw->init_walk_functional(address);

    #ifdef DEBUG_TLB
      std::cout << " Allocate " << address << " at level: " << level << " with page_size" << page_size  <<  std::endl;
    #endif


    IntPtr vpn = address >> page_size;

    bool eviction;
    IntPtr evict_addr;
    CacheBlockInfo evict_block_info;

    IntPtr tag;
    UInt32 set_index;
    //std::cout << "Miss at level" << level << " UTR heuristic: " << m_utr->getHeur() << std::endl;
    

      
    m_cache.splitAddressTLB(address, tag, set_index, page_size);
    m_cache.insertSingleLineTLB(address, NULL, &eviction, &evict_addr, &evict_block_info, NULL, now, NULL,CacheBlockInfo::block_type_t::NON_PAGE_TABLE,page_size);
     
    if(eviction){ // Runs only if L1 translation entry is evicted to the L2 TLB
        if(utopia_enabled){
          if((m_utopia->getHeurSecondary() == Utopia::utopia_heuristic::tlb) && level == 1){
                
                  m_next_level->m_cache.splitAddressTLB(address, tag, set_index, page_size); //@kanellok check pressure at L2 TLB if a an evicted translation from L1 will evict a translation from L2 
                  

                  if(tlb_pressure_cnt.find(set_index) != tlb_pressure_cnt.end() ){ // @kanellok Track the pressure at each TLB set

                      tlb_pressure_cnt[set_index] += 1;
                  }
                  else {
                      tlb_pressure_cnt[set_index] = 1;
                  }

                  if(tlb_pressure_cnt[set_index] > m_utopia->getTLBthr()){
                      tlb_pressure_cnt[set_index] = 0;
                      if(page_size == 12)
                         m_utr_4KB->allocate(address,now, m_core_id);
                      if(page_size == 21)
                         m_utr_2MB->allocate(address,now,m_core_id);
                  }
              
          }
        }
    }
    #ifdef DEBUG_TLB
      if(eviction)
       std::cout << " Evicted " << evict_addr << " from level: " << level << " with page_size" << page_size  <<  std::endl;
    #endif
      if (eviction && m_next_level)
          m_next_level->allocate(evict_addr, now, level+1);


    return ;

  }




}
