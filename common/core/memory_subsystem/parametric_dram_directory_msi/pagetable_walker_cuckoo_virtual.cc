#include "pagetable_walker_cuckoo_virtual.h"

#include <ctime>
#include "config.hpp"
#include "pwc.h"
#include "subsecond_time.h"
#include "simulator.h"
#include "core_manager.h"
#include "memory_manager.h"

namespace ParametricDramDirectoryMSI{

    PageTableWalkerCuckooVirtual::PageTableWalkerCuckooVirtual(int core_id, int _page_size, ShmemPerfModel* _m_shmem_perf_model, PWC* _pwc, 
												bool _page_walk_cache_enabled, int d, char* hash_func, int size,
												float rehash_threshold, uint8_t scale,
                    							uint8_t swaps, uint8_t priority):
        PageTableWalker(core_id, _page_size, _m_shmem_perf_model, _pwc, _page_walk_cache_enabled)
    {
		std::cout << "Instantiating Virtual Cuckoo Page Table" << std::endl;
		create_elastic(d, size, &guestElasticCuckooHT, hash_func, rehash_threshold,
                   scale, swaps, priority);
		create_elastic(d, size, &hostElasticCuckooHT, hash_func, rehash_threshold,
                   scale, swaps, priority);

		String name =  "PTW_cuckoo_virtual";
		cuckoo_faults = 0;    
		cuckoo_latency = SubsecondTime::Zero();
		cuckoo_hits = 0;
		utopia_enabled = Sim()->getCfg()->getBool("perf_model/utopia/enabled");

		
		registerStatsMetric(name, core_id, "ptws", &stats.page_walks);
		registerStatsMetric(name, core_id, "hits", &cuckoo_hits);
		registerStatsMetric(name, core_id, "pagefaults", &cuckoo_faults);
		registerStatsMetric(name, core_id, "latency", &cuckoo_latency);
		registerStatsMetric(name, core_id, "utopia_hits", &utopia_hits);
		registerStatsMetric(name, core_id, "utopia_misses", &utopia_misses);


    }

    SubsecondTime PageTableWalkerCuckooVirtual::init_walk(IntPtr address,UtopiaCache* shadow_cache, CacheCntlr *_cache, Core::lock_signal_t _lock_signal, Byte* _data_buf, UInt32 _data_length, bool _modeled, bool _count)
	{
		// Setup the step walk functions
		stats.page_walks++;
		bzero(&current_lat, sizeof(current_lat));

		cache = _cache;
		modeled = _modeled;
		count = _count;
		data_buf = _data_buf;
		vpn = address >> page_size;
		data_length = _data_length;
		lock_signal = _lock_signal;
		latency = SubsecondTime::Zero();
		
		//printf("Page Walk latency %ld \n",SubsecondTime::divideRounded(current_lat,period));

		accessTable(address,false);

		cuckoo_latency += latency;

		return latency;
	};

    void PageTableWalkerCuckooVirtual::accessTable(IntPtr address,bool nested)
    {

		 TranslationResult utopia_result;
		 
		 elem_t elem;
		 elem.valid = 1;
		 elem.value = address >> page_size;
		 std::vector<elem_t> accessedAddresses;

		 if(nested)
		 	accessedAddresses = find_elastic_ptw(&elem, &hostElasticCuckooHT);
		 else 
		 	accessedAddresses = find_elastic_ptw(&elem, &guestElasticCuckooHT);

		bool found = false;
		int nest_hit =0;


		for(elem_t elem: accessedAddresses){
			
			if(elem.valid)
			{
				found = true;
				cuckoo_hits++;
				break;
			}
		}

		if(!found) cuckoo_faults++;

		SubsecondTime maxLat = SubsecondTime::Zero();

		for(elem_t addr: accessedAddresses)
		{
			SubsecondTime t_start = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
			IntPtr cache_address = addr.value & (~((64 - 1))); 
			
			cache->processMemOpFromCore(
				lock_signal,
				Core::mem_op_t::READ,
				cache_address, 0,
				data_buf, data_length,
				modeled,
				count,CacheBlockInfo::block_type_t::PAGE_TABLE, SubsecondTime::Zero());

			SubsecondTime t_end = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);

			
			ComponentLatency hash_latency = ComponentLatency(Sim()->getCoreManager()->getCoreFromID(core_id)->getDvfsDomain(),4);

			ComponentLatency pwc_latency = ComponentLatency(Sim()->getCoreManager()->getCoreFromID(core_id)->getDvfsDomain(),4); // We assume that the cache always finds the 4KB way or the 2MB way 

			if(!found && (t_end - t_start+hash_latency.getLatency()+pwc_latency.getLatency() > maxLat))
				maxLat = (t_end - t_start)+hash_latency.getLatency()+pwc_latency.getLatency();
			
			if(found && addr.valid)
				maxLat = t_end - t_start+hash_latency.getLatency()+pwc_latency.getLatency();
			
			if(!nested){ 	
			
				if(utopia_enabled){

					MemoryManager* mmanager = (MemoryManager* ) (Sim()->getCoreManager()->getCoreFromID(core_id)->getMemoryManager());
					utopia_result = mmanager->accessUtopiaSubsystem(lock_signal,addr.value, data_buf, data_length, modeled, count);
					
					if(utopia_result.hitwhere == TranslationHitWhere::UTR_MISS){
						   Utopia *m_utopia;
						   UTR* m_utr;
						   m_utopia = Sim()->getUtopia();
   						   m_utr = m_utopia->getUtr(0);
						   SubsecondTime t_start = getShmemPerfModel()->getElapsedTime(ShmemPerfModel::_USER_THREAD);
						   m_utr->allocate(addr.value,t_start,core_id);
						   utopia_misses++;
						   accessTable(addr.value,true);
					}
					else {
						utopia_hits++;
						latency += utopia_result.latency;
					}
				}
				else accessTable(addr.value,true);
			
			}
		}

		current_lat = maxLat;
		latency += maxLat;


    }



}